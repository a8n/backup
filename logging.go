// SPDX-License-Identifier: PostgreSQL

// Package backup integrates with TRMM to provide backups.
package backup

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"time"
)

type LoggingConfig struct {
	// https://github.com/rs/zerolog#leveled-logging
	Level string `enum:"debug,info,warn,error" default:"info"`
	Type  string `enum:"json,console" default:"console"`
}

func SetLogging(l *LoggingConfig) {
	zerolog.TimeFieldFormat = time.RFC3339Nano
	log.Logger = log.Output(zerolog.ConsoleWriter{
		Out:        os.Stderr,
		TimeFormat: "15:04:05.000000",
		NoColor:    !LOG_COLOR,
	})
	// Add the caller filename to the output
	log.Logger = log.With().Caller().Logger()

	switch l.Level {
	case "trace":
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case "warn":
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	case "error":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	default:
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		log.Debug().
			Int8("level", int8(zerolog.GlobalLevel())).
			Msg("Log level not defined. Using defaults.")
	}
}
