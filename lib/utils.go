// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// Package lib is a general library of utilities and things.
package lib

import (
	"encoding/json"
	"fmt"
	"github.com/TylerBrock/colorjson"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/rs/zerolog/log"
)

// FormatJSON converts a byte array to formatted string.
func FormatJSON(j []byte) (string, error) {
	var err error
	var obj map[string]interface{}
	var s []byte

	err = json.Unmarshal(j, &obj)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error unmarshalling the JSON response")
		return "", err
	}
	//f := colorjson.NewFormatter()
	//f.Indent = 4
	//f.DisabledColor = false
	//s, err = f.Marshal(obj)

	s, err = colorjson.Marshal(obj)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error marshaling the JSON object")
		return "", err
	}
	return fmt.Sprintln(string(s)), nil
}

// GetLatestTagFromRepository will get the latest tag from the git repository.
// See https://github.com/src-d/go-git/issues/1030#issuecomment-443679681
func GetLatestTagFromRepository(repo *git.Repository) (string, error) {
	tagRefs, err := repo.Tags()
	if err != nil {
		return "", err
	}

	var latestTagCommit *object.Commit
	var latestTagName string
	err = tagRefs.ForEach(func(tagRef *plumbing.Reference) error {
		revision := plumbing.Revision(tagRef.Name().String())
		tagCommitHash, err := repo.ResolveRevision(revision)
		if err != nil {
			return err
		}

		commit, err := repo.CommitObject(*tagCommitHash)
		if err != nil {
			return err
		}

		if latestTagCommit == nil {
			latestTagCommit = commit
			latestTagName = tagRef.Name().String()
		}

		if commit.Committer.When.After(latestTagCommit.Committer.When) {
			latestTagCommit = commit
			latestTagName = tagRef.Name().String()
		}

		return nil
	})
	if err != nil {
		return "", err
	}

	return latestTagName, nil
}
