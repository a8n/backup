// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// Package crypto contains code and structs shared between the manage and agent modules.
package crypto

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"

	"github.com/go-yaml/yaml"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/nacl/box"
)

// KeyPair manages the encryption routines for one public/private key pair.
type KeyPair struct {
	Public  *Key
	Private *Key
	err     error `default:"nil"`
	bytes   []byte
}

// PEMPair is a pair of encryption keys as strings in PEM, base64 or other format. This is used to
// pass key pairs to other modules easily.
type PEMPair struct {
	Public  string `json:"public_key"`
	Private string `json:"private_key"`
}

// Key represents a single key and its various forms.
type Key struct {
	banner string
	pem    []byte
	key    *[32]byte
	err    error `default:"nil"`
	bytes  []byte
}

// Message starts a new encrypted Message request.
type Message struct {
	message   []byte
	keys      *KeyPair
	encrypted []byte
	decrypted []byte
	bytes     []byte
	err       error `default:"nil"`
}

const (
	PublicKeyBanner  = "PUBLIC KEY"
	PrivateKeyBanner = "PRIVATE KEY"
	// TODO: What is the appropriate heading/banner?
	//X25519PrivateKeyBanner = "A8N BACKUP X25519 PRIVATE KEY"
	//X25519PublicKeyBanner  = "A8N BACKUP X25519 PUBLIC KEY"
	//Ed25519PrivateKeyBanner = "A8N BACKUP ED25519 PRIVATE KEY"
	//Ed25519PublicKeyBanner  = "A8N BACKUP ED25519 PUBLIC KEY"
)

// NewKeyPair starts a new encryption request
func NewKeyPair(name string) *KeyPair {
	return &KeyPair{
		Public: &Key{
			banner: name + " " + PublicKeyBanner,
		},
		Private: &Key{
			banner: name + " " + PrivateKeyBanner,
		},
	}
}

// String will convert the Key structure to a string.
func (kp *KeyPair) String() (string, string) {
	if kp.err != nil {
		return "", ""
	}

	// FIXME: Take into account both keys.
	kp.Bytes()
	if kp.err != nil {
		return "", ""
	}

	return string(kp.Public.bytes), string(kp.Private.bytes)
}

// Bytes will convert the TRMM structure to a []byte slice.
func (kp *KeyPair) Bytes() []byte {
	if kp.err != nil {
		return nil
	}

	// FIXME: Take into account both keys.
	if len(kp.bytes) == 0 {
		// Default is HCL format
		kp.ToHCL()
		if kp.err != nil {
			return nil
		}
	}

	return kp.bytes
}

// ToHCL will convert the Key structure to HCL.
// TODO: Is this needed?
func (kp *KeyPair) ToHCL() *KeyPair {
	if kp.err != nil {
		return kp
	}

	f := hclwrite.NewEmptyFile()
	gohcl.EncodeIntoBody(kp, f.Body())
	kp.bytes = f.Bytes()

	// When the wrong struct is passed, no error is produced and f.Bytes() returns
	// and empty string.
	if len(kp.bytes) == 0 {
		log.Debug().
			Str("bytes_length", string(f.Bytes())).
			Msg("Converting to HCL failed. Returning an empty string.")
	}

	return kp
}

// ToJSON will convert the Key structure to JSON.
func (kp *KeyPair) ToJSON() *KeyPair {
	if kp.err != nil {
		return kp
	}

	var err error
	kp.bytes, err = json.MarshalIndent(kp, "", "  ")
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to Marshal JSON.")
		kp.err = err
		return kp
	}

	return kp
}

// ToYAML will convert the Key structure to YAML.
func (kp *KeyPair) ToYAML() *KeyPair {
	if kp.err != nil {
		return kp
	}

	var err error
	kp.bytes, err = yaml.Marshal(kp)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to Marshal YAML.")
		kp.err = err
		return kp
	}

	return kp
}

// HasErrors returns true if errors are present.
func (kp *KeyPair) HasErrors() bool {
	return kp.err != nil
}

// Errors returns errors present, or nil otherwise.
func (kp *KeyPair) Error() error {
	return kp.err
}

// ErrorMessage returns the error message.
func (kp *KeyPair) ErrorMessage() string {
	return kp.err.Error()
}

// GenerateKeyPair will generate a public/private encryption key pair.
func (kp *KeyPair) GenerateKeyPair() *KeyPair {
	if kp.err != nil {
		return kp
	}

	var err error
	kp.Public.key, kp.Private.key, err = box.GenerateKey(rand.Reader)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to generate public/private key pair.")
		kp.err = err
		return kp
	}

	return kp
}

// FromPEM will set the public/private keys form the string in PEM format.
//func (kp *KeyPair) SetFromPEM(public string, private string) *KeyPair {
func (kp *KeyPair) FromPEM(pair PEMPair) *KeyPair {
	if kp.err != nil {
		return kp
	}

	kp.Public.pem = []byte(pair.Public)
	kp.Private.pem = []byte(pair.Private)
	if kp.Unmarshal(); kp.err != nil {
		log.Error().
			Err(kp.err).
			Msg("Failed Failed to set key pair from PEM.")
		return kp
	}

	return kp
}

// ToPEM will get the public/private keys and return them as a PEMPair
//func (kp *KeyPair) ToPEM(public string, private string) *KeyPair {
func (kp *KeyPair) ToPEM(pair *PEMPair) *KeyPair {
	if kp.err != nil {
		return kp
	}

	if kp.Marshal(); kp.err != nil {
		log.Error().
			Err(kp.err).
			Msg("Failed to marshal key pair to PEM format.")
		return kp
	}
	pair.Public = string(kp.Public.pem)
	pair.Private = string(kp.Private.pem)
	return kp
}

// Unmarshal will decode a PEM and save the key.
func (kp *KeyPair) Unmarshal() *KeyPair {
	if kp.err != nil {
		return kp
	}

	// It's possible to have only one key; the other key will be empty.
	if len(kp.Public.pem) > 0 {
		kp.Public.Unmarshal()
	}
	if len(kp.Private.pem) > 0 {
		kp.Private.Unmarshal()
	}
	return kp
}

// Marshal will encode a key to PEM format.
func (kp *KeyPair) Marshal() *KeyPair {
	if kp.err != nil {
		return kp
	}

	// It's possible to have only one key; the other key will be empty.
	if kp.Public.key != nil {
		kp.Public.Marshal()
	}
	if kp.Private.pem != nil {
		kp.Private.Marshal()
	}
	return kp
}

// encodeBase64 will encode the PEM format to base64.
func (kp *KeyPair) encodeBase64() PEMPair {
	if kp.err != nil {
		return PEMPair{}
	}
	return PEMPair{
		Public:  kp.Public.encodeBase64(),
		Private: kp.Private.encodeBase64(),
	}
}

/**
 * Methods for a single key.
 */

// Unmarshal will decode a PEM and return the key.
func (k *Key) Unmarshal() *Key {
	if k.err != nil {
		return k
	}

	var rest []byte
	var pb *pem.Block
	var err error

	if len(k.pem) == 0 {
		k.err = errors.New("PEM is empty (0 bytes)")
		log.Error().
			Err(err).
			Int("length", len(k.pem)).
			Msg("PEM is empty (0 bytes).")
		return k
	}

	pb, rest = pem.Decode(k.pem)
	if pb == nil {
		k.err = errors.New("failed to decode PEM block")
		log.Error().
			Err(err).
			Str("remaining_content", string(rest)).
			Msg("Failed to decode PEM block.")
		return k
	}

	if len(pb.Bytes) != 32 {
		k.err = errors.New("key is not 32 bytes")
		log.Error().
			Err(err).
			Int("length", len(pb.Bytes)).
			Msg("Key is not 32 bytes.")
		return k
	}
	k.key = (*[32]byte)(pb.Bytes[:32])

	return k
}

// Marshal will encode a key to PEM format.
func (k *Key) Marshal() *Key {
	if k.err != nil {
		return k
	}

	priv := string(pem.EncodeToMemory(&pem.Block{
		Type:  k.banner,
		Bytes: k.key[:32],
	}))
	k.pem = []byte(priv)

	return k
}

// encodeBase64 will encode the key from PEM format to base64.
func (k *Key) encodeBase64() string {
	if k.err != nil {
		return ""
	}

	return base64.StdEncoding.EncodeToString(k.pem)
}

// String will return the keys in PEM format.
func (k *Key) String() string {
	if k.err != nil {
		return ""
	}

	if k.key == nil {
		log.Warn().
			Str("key", fmt.Sprintf("%#v", k.key)).
			Msg("Key is not set. Returning the empty string.")
		return ""
	}
	priv := string(pem.EncodeToMemory(&pem.Block{
		Type:  k.banner,
		Bytes: k.key[:32],
	}))
	k.pem = []byte(priv)

	return string(k.pem)
}

// Reader will return the raw (binary) key.
// TODO: This may not be needed.
func (k *Key) Reader() []byte {
	if k.err != nil {
		return nil
	}

	return k.bytes
}

// Errors returns errors present, or nil otherwise.
func (k *Key) Error() error {
	return k.err
}

// HasErrors returns true if errors are present.
func (k *Key) HasErrors() bool {
	return k.err != nil
}

// ErrorMessage returns the error message.
func (k *Key) ErrorMessage() string {
	return k.err.Error()
}

/**
 * Encrypted messages.
 */

// NewMessage starts a Message request.
func NewMessage() *Message {
	return &Message{
		keys: NewKeyPair(""),
	}
}

// SetMessage sets the message key for the encryption.
func (m *Message) SetMessage(message []byte) *Message {
	m.message = message
	return m
}

// SetPublicKey sets the public key for the encryption.
func (m *Message) SetPublicKey(public *Key) *Message {
	m.keys.Public = public
	return m
}

// SetPrivateKey sets the private key for the encryption.
func (m *Message) SetPrivateKey(private *Key) *Message {
	m.keys.Private = private
	return m
}

// Encrypt will Encrypt the message.
// decrypted > encrypted > EncodeBase64 > transport > DecodeBase64 > encrypted > decrypted
func (m *Message) Encrypt(message []byte) *Message {
	if m.err != nil {
		return m
	}

	log.Debug().
		Str("method", "Encrypt").
		Int("message_length", len(m.message)).
		Int("decrypted_length", len(m.decrypted)).
		Int("bytes_length", len(m.bytes)).
		Bool("has_errors", m.HasErrors()).
		Msg("Encrypting message.")

	if m.keys.Private == nil || m.keys.Public == nil {
		log.Error().
			Msg("Private and/or public key is not set.")
		m.err = errors.New("either public and/or private key is not set")
		return m
	}

	if m.keys.Private.key == nil || m.keys.Public.key == nil {
		log.Error().
			Msg("Private and/or public key is nil.")
		m.err = errors.New("either public and/or private key is nil")
		return m
	}

	if len(message) > 0 && len(m.message) > 0 {
		log.Error().
			Str("m.message", string(m.message)).
			Str("message_raw", string(message)).
			Msg("Message to Encrypt is already set.")
		m.err = errors.New("message to Encrypt is already set")
		return m
	}

	if len(message) == 0 && len(m.message) == 0 {
		log.Error().
			Str("m.message", string(m.message)).
			Str("message_raw", string(message)).
			Msg("Message to Encrypt is empty.")
		m.err = errors.New("message to Encrypt is empty")
		return m
	}

	if len(message) > 0 {
		m.message = message
	}

	// You must use a different nonce for each message you encrypt with the
	// same key. Since the nonce here is 192 bits long, a random value
	// provides a sufficiently small probability of repeats.
	var nonce [24]byte
	if _, err := io.ReadFull(rand.Reader, nonce[:]); err != nil {
		log.Error().
			Err(err).
			Msg("Failed to create the nonce.")
		m.err = err
		return m
	}

	// This encrypts msg and appends the result to the nonce.
	// See https://pkg.go.dev/golang.org/x/crypto/nacl/box#example-package
	m.encrypted = box.Seal(nonce[:], m.message, &nonce, m.keys.Public.key, m.keys.Private.key)

	return m
}

// Decrypt will Encrypt the message.
// decrypted > encrypted > EncodeBase64 > transport > DecodeBase64 > encrypted > decrypted
func (m *Message) Decrypt(encrypted []byte) *Message {
	if m.err != nil {
		return m
	}

	if m.keys.Private == nil || m.keys.Public == nil {
		log.Error().
			Msg("Private and/or public key is not set.")
		m.err = errors.New("either public and/or private key is not set")
		return m
	}

	if len(encrypted) > 0 && len(m.encrypted) > 0 {
		log.Error().
			Str("m.encrypted", string(m.encrypted)).
			Str("encrypted", string(encrypted)).
			Msg("Message to decrypt is already set.")
		m.err = errors.New("message to decrypt is already set")
		return m
	}

	if len(encrypted) == 0 && len(m.encrypted) == 0 {
		log.Error().
			Str("m.encrypted", string(m.encrypted)).
			Str("encrypted", string(encrypted)).
			Msg("Message to decrypt is empty.")
		m.err = errors.New("message to decrypt is empty")
		return m
	}

	if len(encrypted) > 0 {
		m.encrypted = encrypted
	}

	if m.keys.Public.key == nil || m.keys.Private.key == nil {
		m.err = errors.New("public or private decryption keys were not provided")
		log.Error().
			Err(m.err).
			Str("public_key_pem", string(m.keys.Public.pem)).
			Str("private_key_pem", string(m.keys.Private.pem)).
			Msg("Decryption keys were not provided.")
		return m
	}

	// The recipient can decrypt the message using their private key and the
	// sender's public key. When you decrypt, you must use the same nonce you
	// used to encrypt the message. One way to achieve this is to store the
	// nonce alongside the encrypted message. Above, we stored the nonce in the
	// first 24 bytes of the encrypted text.
	// See https://pkg.go.dev/golang.org/x/crypto/nacl/box#example-package
	var decryptNonce [24]byte
	copy(decryptNonce[:], m.encrypted[:24])
	var ok bool
	m.decrypted, ok = box.Open(nil, m.encrypted[24:], &decryptNonce, m.keys.Public.key, m.keys.Private.key)
	if !ok {
		m.err = errors.New("failed to decrypt the message")
		log.Error().
			Err(m.err).
			Msg("Failed to decrypt the message.")
		return m
	}

	// FIXME: Return the bytes instead of assigning them.
	m.bytes = m.decrypted
	return m
}

// EncodeBase64 will base64 encode the encrypted message.
func (m *Message) EncodeBase64() *Message {
	if m.err != nil {
		return m
	}
	log.Debug().
		Msg("Encoding message to base64.")

	if len(m.bytes) > 0 {
		// For some reason, zerolog does not output if the string size is too large.
		//fmt.Printf("%#v\n", m)
		log.Warn().
			Str("method", "EncodeBase64").
			Int("message_length", len(m.message)).
			Int("decrypted_length", len(m.decrypted)).
			Int("bytes_length", len(m.bytes)).
			Bool("has_errors", m.HasErrors()).
			Msg("Overwriting existing message.bytes content.")
	}

	m.bytes = []byte(base64.StdEncoding.EncodeToString(m.encrypted))
	return m
}

// DecodeBase64 will base64 decode the encrypted message.
func (m *Message) DecodeBase64() *Message {
	if m.err != nil {
		return m
	}
	log.Debug().
		Msg("Decoding message from base64.")

	if len(m.message) == 0 {
		log.Error().
			Str("method", "DecodeBase64").
			Int("message_length", len(m.message)).
			Int("decrypted_length", len(m.decrypted)).
			Int("bytes_length", len(m.bytes)).
			Bool("has_errors", m.HasErrors()).
			Msg("Message is 0 bytes.")
		m.err = errors.New("message is 0 bytes")
		return m
	}

	m.encrypted, m.err = base64.StdEncoding.DecodeString(string(m.message))
	if m.err != nil {
		log.Error().
			Msg("Failed to base64 decode the message.")
		m.err = errors.New("failed to base64 decode the message")
		return m
	}
	// Cannot use the key "message"
	// See https://github.com/rs/zerolog/issues/172
	log.Trace().
		Str("message_string", string(m.message)).
		Int("message_length", len(string(m.message))).
		Int("base64_length", len(m.message)).
		Int("binary_length", len(m.encrypted)).
		Msg("Base64 decoded message.")
	return m
}

// String will return the encoded message as a string.
func (m *Message) String() string {
	if m.err != nil {
		return ""
	}

	if len(m.bytes) == 0 {
		m.err = errors.New("message is empty")
		log.Error().
			Err(m.err).
			Msg("Message is empty.")
		return ""
	}

	return string(m.bytes)
}

// Errors returns errors present, or nil otherwise.
func (m *Message) Error() error {
	return m.err
}

// HasErrors returns true if errors are present.
func (m *Message) HasErrors() bool {
	return m.err != nil
}

// ErrorMessage returns the error message.
func (m *Message) ErrorMessage() string {
	return m.err.Error()
}
