// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// Package pkg deals with downloading and installing packages from git repositories.
// See also [just-install](https://github.com/just-install/just-install).
package pkg

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
	"unicode/utf8"

	"github.com/cavaliergopher/grab/v3"
	"github.com/google/go-github/v45/github"
	"github.com/mholt/archiver/v4"
	"github.com/rs/zerolog/log"

	ufs "gitlab.com/a8n/backup/lib/fs"
)

// pkg is a software package to install.
type pkg struct {
	repo        repo
	release     asset
	checksum    checksum
	application application
	install     install
	err         error
}

// repo is the repository.
type repo struct {
	name    string
	tag     string
	version string
}

// asset is a specific asset that was released.
type asset struct {
	filename    string `default:""`
	url         string `default:""`
	contentType string `default:""`
}

// checksum represents the checksum downloaded from the repository.
type checksum struct {
	filename    string `default:""`
	url         string `default:""`
	contentType string `default:"application/octet-stream"`
	sumStr      string
	sum         []byte
}

// application represents the application attributes.
type application struct {
	name        string
	provider    string
	owner       string
	repository  string
	filenameFmt string
}

// install is the installation specific information.
type install struct {
	filename string
	dir      string
	bin      string
	ext      string
}

// software is a list of software that can be installed.
var software = map[string]application{
	"restic": {
		name:        "restic",
		provider:    "github",
		owner:       "restic",
		repository:  "restic",
		filenameFmt: "{{.name}}_{{.version}}_{{.os}}_{{.arch}}",
	},
}

// NewPkg starts a new pkg request.
func NewPkg(application string) *pkg {
	p := &pkg{}
	switch runtime.GOOS {
	case "windows":
		p.install.ext = ".exe"
	case "windows_nt":
		p.install.ext = ".exe"
	}

	sw, ok := software[application]
	if !ok {
		p.err = errors.New(fmt.Sprintf("unknown application: %s", application))
		log.Error().
			Err(p).
			Str("application", application).
			Msg("Unknown application to install")
		return p
	}
	p.application = sw
	return p
}

// GetRelease will get the latest release information from the provider.
func (p *pkg) GetRelease() *pkg {
	if p.err != nil {
		return p
	}

	var release *github.RepositoryRelease
	client := github.NewClient(nil)
	ctx := context.Background()
	release, _, p.err = client.Repositories.GetLatestRelease(ctx, p.application.owner, p.application.repository)
	if p.err != nil {
		log.Error().
			Err(p).
			Msg("Failed to get latest release for the repo.")
		return p
	}

	p.repo.name = *release.Name
	p.repo.tag = *release.TagName
	// FIXME: Remove the leading "v"
	r, size := utf8.DecodeRuneInString(*release.TagName)
	if r == utf8.RuneError {
		p.err = errors.New(string(utf8.RuneError))
		log.Error().
			Err(p).
			Str("tag_name", *release.TagName).
			Msg("Failed to get rune (char) size of release tag name(version).")
		return p
	}

	p.repo.version = (*release.TagName)[size:]

	// FIXME: Clean up the logging.
	log.Info().
		Str("name", p.repo.name).
		Str("tag_name", p.repo.tag).
		Str("version", p.repo.version).
		Msg("Release")
	log.Debug().
		Str("url", *release.URL).
		Str("assets_url", *release.AssetsURL).
		Msg("Release")

	return p.GetLatest(release)
}

// GetLatest will get the files for the latest release.
func (p *pkg) GetLatest(r *github.RepositoryRelease) *pkg {
	re := regexp.MustCompile(fmt.Sprintf("%s_%s", runtime.GOOS, runtime.GOARCH))
	for _, asset := range r.Assets {
		if re.Match([]byte(*asset.Name)) {
			log.Info().
				Str("url", *asset.BrowserDownloadURL).
				Str("name", *asset.Name).
				Msg("assets")
			p.release.filename = *asset.Name
			p.release.url = *asset.BrowserDownloadURL
			p.release.contentType = *asset.ContentType
		}

		if *asset.Name == "SHA256SUMS" {
			log.Info().
				Str("url", *asset.BrowserDownloadURL).
				Str("name", *asset.Name).
				Msg("assets")
			p.checksum.filename = *asset.Name
			p.checksum.url = *asset.BrowserDownloadURL
			p.checksum.contentType = *asset.ContentType
		}
	}
	log.Debug().
		Str("p", fmt.Sprintf("%#v", p)).
		Msg("Pkg Config")

	/*
		t := template.Must(template.New("name").Parse(p.application.filenameFmt))
		buf := bytes.NewBuffer(nil)
		p.err = t.Execute(buf, map[string]interface{}{
			"name":    p.application.name,
			"version": p.application.name,
			"os":      strings.ToLower(runtime.GOOS),
			"arch":    strings.ToLower(runtime.GOARCH),
		})
		if p.err != nil {
			log.Error().
				Err(p).
				Msg("Error executing version template.")
			return p
		}
		// buf.String() contains the os and arch which generally is not wanted.
		//p.install.filename = buf.String()
	*/
	p.install.filename = p.application.name + p.install.ext

	return p
}

// GetChecksum downloads the checksum file and extracts the checksum for the release
func (p *pkg) GetChecksum() *pkg {
	if p.err != nil {
		return p
	}

	var req *grab.Request
	req, p.err = grab.NewRequest(".", p.checksum.url)
	if p.err != nil {
		log.Error().
			Err(p).
			Msg("Failed to create grab request.")
		return p
	}

	// Save the file in memory.
	req.NoStore = true
	resp := grab.DefaultClient.Do(req)
	if p.err = resp.Err(); p.err != nil {
		log.Error().
			Err(p).
			Msg("Failed to download checksums into memory.")
		return p
	}
	var sum []byte
	sum, p.err = resp.Bytes()
	if p.err != nil {
		log.Error().
			Err(p).
			Msg("Failed to read checksums that were downloaded.")
		return p
	}
	re := regexp.MustCompile(fmt.Sprintf("([a-f0-9]+)\\s+%s", p.release.filename))
	matches := re.FindSubmatch(sum)
	if len(matches) != 2 {
		p.err = errors.New("could not extract checksum from downloaded checksums")
		log.Error().
			Err(p).
			Int("matches_length", len(matches)).
			Str("matches", fmt.Sprintf("%#v", matches)).
			Msg("Could not extract checksum from downloaded checksums.")
		return p
	}
	sumStr := string(matches[1])
	p.checksum.sumStr = sumStr
	p.checksum.sum, p.err = hex.DecodeString(sumStr)
	if p.err != nil {
		log.Error().
			Err(p).
			Msg("Failed to decode checksum")
		return p
	}

	log.Info().
		Str("checksum", p.checksum.sumStr).
		Bool("is_complete", resp.IsComplete()).
		Str("filename", p.checksum.filename).
		Msg("Downloaded checksum file.")

	return p
}

/*
// copy will copy a file stream.
func copy(src *os.File, dst *os.File) (err error) {
	inFile, err = os.Open(p.release.filename)
	defer func(inFile *os.File) {
		err := inFile.Close()
		if err != nil {
			log.Error().
				Err(err).
				Msg("Failed to close source (bzip2) file after reading.")
			return
		}
	}(inFile)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to open source (bzip2) file for reading.")
		return err
	}

	var outFile *os.File
	outFile, err = os.Create(fp)
	defer func(outFile *os.File) {
		err := outFile.Close()
		if err != nil {
			log.Error().
				Err(err).
				Msg("Failed to close destination after writing.")
			return
		}
	}(outFile)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to open destination file for writing.")
		return err
	}

	return nil
}
*/

// Download will download the latest release file
func (p *pkg) Download() *pkg {
	if p.err != nil {
		return p
	}

	var req *grab.Request
	// FIXME: Use os.MkdirTemp for temporary directories.
	req, p.err = grab.NewRequest(".", p.release.url)
	if p.err != nil {
		log.Error().
			Err(p).
			Msg("Failed to create grab request")
		return p
	}
	req.Filename = p.release.filename
	req.SetChecksum(sha256.New(), p.checksum.sum, false)
	resp := grab.DefaultClient.Do(req)
	if p.err = resp.Err(); p.err != nil {
		log.Error().
			Err(p).
			Str("checksum_string", p.checksum.sumStr).
			//Bytes("checksum_bytes", p.checksum.sum).
			Msg("Failed to download file")
		return p
	}
	log.Info().
		Bool("is_complete", resp.IsComplete()).
		Msg("Download complete")

	return p
}

// Decompress will decompress the file using bzip2
// https://snyk.io/research/zip-slip-vulnerability
// https://github.com/snyk/zip-slip-vulnerability
// Useful decompression using pipes:
// https://github.com/pedroalbanese/bzip2/blob/main/cmd/bzip2/main.go
// https://stackoverflow.com/questions/36228655/golang-decompress-bz2-in-on-goroutine-consume-in-other-goroutine
func (p *pkg) Decompress() *pkg {
	if p.err != nil {
		return p
	}

	fullPath := filepath.Join(p.install.bin, p.install.filename)
	if ufs.Exists(fullPath) {
		log.Info().
			Str("full_path", fullPath).
			Msg("Destination file exists. Not overwriting.")
		return p
	}

	var fsys fs.FS
	fsys, p.err = archiver.FileSystem(p.release.filename)
	if p.err != nil {
		log.Error().
			Err(p).
			Str("archive", p.release.filename).
			Msg("Error opening the archive file")
		return p
	}

	// Get filename from the ZIP filename
	ext := filepath.Ext(p.release.filename)
	basename := strings.TrimSuffix(p.release.filename, ext)

	p.err = fs.WalkDir(fsys, `.`, func(path string, f fs.DirEntry, err error) error {
		if f.IsDir() {
			return nil
		}

		fh, err := fsys.Open(path)
		if err != nil {
			return err
		}
		defer func(fh fs.File) {
			err := fh.Close()
			if err != nil {
				log.Error().
					Err(err).
					Msg("Error closing file")
			}
		}(fh)

		log.Debug().
			Str("path", path).
			Str("basename", basename).
			Str("file_ext", p.install.ext).
			Msg("Searching archive.")

		if path == basename+p.install.ext {
			log.Info().
				Str("file", path).
				Msg("Found file in archive. Extracting file.")

			var outFile *os.File
			outFile, err = os.Create(fullPath)
			defer func(outFile *os.File) {
				err := outFile.Close()
				if err != nil {
					log.Error().
						Err(err).
						Msg("Failed to close destination after writing.")
					return
				}
			}(outFile)
			if err != nil {
				log.Error().
					Err(err).
					Msg("Failed to open destination file for writing.")
				return err
			}

			var written int64
			written, p.err = io.Copy(outFile, fh)
			if err != nil {
				log.Error().
					Err(err).
					Int64("bytes_written", written).
					Str("in_file", path).
					Str("out_file", fullPath).
					Msg("Failed to copy file from archive to install location.")
				return err
			}

			log.Info().
				Int64("bytes_written", written).
				Str("file", path).
				Str("full_path", fullPath).
				Msg("Extracted file from archive.")
		}

		return nil
	})
	if p.err != nil {
		log.Error().
			Err(p).
			Str("archive", p.release.filename).
			Msg("Error reading the files in the archive.")
		return p
	}

	// FIXME: Verify the checksums match
	return p
}

// SetLocation will set the installation location of the file.
func (p *pkg) SetLocation(location string) *pkg {
	if p.err != nil {
		return p
	}

	p.install.dir = location
	p.install.bin = filepath.Join(p.install.dir, "bin")
	log.Debug().
		Str("install_dir", p.install.dir).
		Str("install_bin", p.install.dir).
		Msg("Installation dirctories.")

	return p
}

// Install to the given directory
func Install(application string, location string) (err error) {
	newPkg := NewPkg(application)

	_ = newPkg.SetLocation(location).
		GetRelease().
		GetChecksum().
		Download().
		Decompress()
	if newPkg.err != nil {
		log.Error().
			Err(newPkg.err).
			Msg("Failed to decompress the file.")
		return newPkg.err
	}

	if !ufs.Exists(filepath.Join(newPkg.install.bin, newPkg.install.filename)) {
		err = errors.New("application file does not exist in installation directory")
		log.Error().
			Err(err).
			Str("filename", filepath.Join(newPkg.install.bin, newPkg.install.filename)).
			Str("install_bin", newPkg.install.bin).
			Str("install_filename", newPkg.install.filename).
			Msg("Application file does not exist in installation directory.")
		return err
	}

	if runtime.GOOS != "windows" && runtime.GOOS != "windows_nt" {
		err = ufs.SetExecute(filepath.Join(newPkg.repo.name, "restic"))
		if err != nil {
			log.Error().
				Err(err).
				Str("file", filepath.Join(newPkg.repo.name, "restic")).
				Msg("Failed to make the file executable (chmod).")
			return err
		}
	}

	return nil
}

// Errors returns errors present, or nil otherwise.
func (p *pkg) Error() string {
	return p.err.Error()
}

// HasErrors returns true if errors are present.
func (p *pkg) HasErrors() bool {
	return p.err != nil
}
