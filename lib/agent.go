// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// Package lib contains code and structs shared between the manage and agent modules.
package lib

// AgentTemplate manages the HCL config for the backup agent.
type AgentTemplate struct {
	Source   Source   `hcl:"source,block" json:"source"`
	Storage  Storage  `hcl:"storage,block" json:"storage"`
	Restic   Restic   `hcl:"restic,block" json:"restic"`
	Tactical Tactical `hcl:"tactical,block" json:"tactical"`
	Hosting  Hosting  `hcl:"hosting,block" json:"hosting,omitempty"`
}

// Source is a list of sources to back up.
type Source struct {
	Paths []string `hcl:"paths,attr" json:"paths"`
}

// Storage is the storage provider for the restic repository.
type Storage struct {
	Provider  string     `hcl:"provider,attr" json:"provider,omitempty" default:"rest"`
	Rest      *Rest      `hcl:"rest,block" json:"rest"`
	Backblaze *Backblaze `hcl:"backblaze,block" json:"backblaze"`
	Azure     *Azure     `hcl:"azure,block" json:"azure"`
	Google    *Google    `hcl:"google,block" json:"google"`
}

// Tactical is for user reference only. It might be used in the future.
type Tactical struct {
	Client *string `hcl:"client,attr" json:"client"`
	Site   *string `hcl:"site,attr" json:"site"`
	Agent  *string `hcl:"agent,attr" json:"agent"`
}

// Hosting is for the Tactical script that runs on the endpoint. It needs to know where
// to download the backup AgentConfig and the format of the URL.
type Hosting struct {
	// TODO: File hosting does not belong in the agent config.
	Upload struct {
		Domain        *string `hcl:"domain"`
		Path          *string `hcl:"path"`
		BasicUsername *string `hcl:"basic_username"`
		BasicPassword *string `hcl:"basic_password"`
		// Filename is the base filename without the os and arch.
		// The actual filename will be {filename}_{GOOS}_{GOARCH}[.ext]
		Filename *string `hcl:"filename"`
	} `hcl:"upload,block" json:"-"`
	Download struct {
		URL           *string `hcl:"url" json:"url"`
		BasicUsername *string `hcl:"basic_username" json:"basic_username"`
		BasicPassword *string `hcl:"basic_password" json:"basic_password"`
		// Filename is the base filename without the os and arch.
		// The actual filename will be {filename}_{GOOS}_{GOARCH}[.ext]
		Filename *string `hcl:"filename" json:"filename"`
	} `hcl:"download,block" json:"download"`
	OS   *string `hcl:"os" json:"os,omitempty"`
	Arch *string `hcl:"arch" json:"arch,omitempty"`
}
