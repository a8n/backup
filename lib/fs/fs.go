// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// Package fs is a general library of filesystem things.
package fs

import (
	"errors"
	"github.com/rs/zerolog/log"
	"os"
)

// Unix file modes
// https://stackoverflow.com/a/42718395
// Note: Creating files/directories on Unix will conform to the umask settings and may not use the mode specified.
// A workaround is to change the mode after the file/directory is created since umask affects the mode during
// creation, not afterwards.
const (
	OS_READ        = 04
	OS_WRITE       = 02
	OS_EX          = 01
	OS_USER_SHIFT  = 6
	OS_GROUP_SHIFT = 3
	OS_OTH_SHIFT   = 0

	OS_USER_R   = OS_READ << OS_USER_SHIFT
	OS_USER_W   = OS_WRITE << OS_USER_SHIFT
	OS_USER_X   = OS_EX << OS_USER_SHIFT
	OS_USER_RW  = OS_USER_R | OS_USER_W
	OS_USER_RWX = OS_USER_RW | OS_USER_X

	OS_GROUP_R   = OS_READ << OS_GROUP_SHIFT
	OS_GROUP_W   = OS_WRITE << OS_GROUP_SHIFT
	OS_GROUP_X   = OS_EX << OS_GROUP_SHIFT
	OS_GROUP_RW  = OS_GROUP_R | OS_GROUP_W
	OS_GROUP_RWX = OS_GROUP_RW | OS_GROUP_X

	OS_OTH_R   = OS_READ << OS_OTH_SHIFT
	OS_OTH_W   = OS_WRITE << OS_OTH_SHIFT
	OS_OTH_X   = OS_EX << OS_OTH_SHIFT
	OS_OTH_RW  = OS_OTH_R | OS_OTH_W
	OS_OTH_RWX = OS_OTH_RW | OS_OTH_X

	OS_ALL_R   = OS_USER_R | OS_GROUP_R | OS_OTH_R
	OS_ALL_W   = OS_USER_W | OS_GROUP_W | OS_OTH_W
	OS_ALL_X   = OS_USER_X | OS_GROUP_X | OS_OTH_X
	OS_ALL_RW  = OS_ALL_R | OS_ALL_W
	OS_ALL_RWX = OS_ALL_RW | OS_GROUP_X
)

// IsFile returns true if path is a file.
func IsFile(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}
	mode := fileInfo.Mode()
	return mode.IsRegular()
}

// IsDir returns true if path is a directory.
func IsDir(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}
	mode := fileInfo.Mode()
	return mode.IsDir()
}

// IsLink returns true if path is a symlink.
func IsLink(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}
	mode := fileInfo.Mode()
	return (mode & os.ModeSymlink) != 0
}

// Exists returns true if the path exists (file or directory)
// https://stackoverflow.com/a/12527546
// https://stefanxo.com/go-anti-patterns-os-isexisterr-os-isnotexisterr/
func Exists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if errors.Is(err, os.ErrNotExist) {
		return false
	}
	return false
}

// SetExecute will set the execution bit for all on Unix without changing the other
// bits.
func SetExecute(path string) (err error) {
	// FIXME: This only work on Unix. Should we test for non-Unix OS?
	if !IsFile(path) {
		return errors.New("path is not a file")
	}
	var info os.FileInfo
	info, err = os.Stat(path)
	mode := info.Mode() | (OS_ALL_R | OS_ALL_X)
	err = os.Chmod(path, mode)
	if err != nil {
		return err
	}
	info, err = os.Stat(path)
	return nil
}

// GetContents will get the contents of a file.
func GetContents(path *string) (contents []byte, err error) {
	if !Exists(*path) {
		log.Error().
			Str("path", *path).
			Msg("PEM file does not exist.")
		return nil, errors.New("pem file does not exist")
	}

	contents, err = os.ReadFile(*path)
	if err != nil {
		log.Error().
			Err(err).
			Str("path", *path).
			Msg("Failed to read the PEM file.")
		return nil, err
	}
	if len(contents) == 0 {
		log.Warn().
			Int("bytes", len(contents)).
			Str("path", *path).
			Msg("File is empty.")
		return contents, nil
	}

	return contents, nil
}
