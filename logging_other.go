// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is not included in the Agent build.
//go:build !agent

// Package backup integrates with TRMM to provide backups.
package backup

var LOG_COLOR = true
