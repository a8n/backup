// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is not included in the Agent build.
//go:build !agent

// Package backup integrates with TRMM to provide backups.
package backup

import (
	"fmt"
	"github.com/rs/zerolog/log"
	ufs "gitlab.com/a8n/backup/lib/fs"
	"gitlab.com/a8n/backup/manage"
	"path/filepath"
	"strings"
)

// Backup is the backup config
type Backup struct {
	CLI    *CLI
	Manage *manage.Manage
	Agent  *manage.Agent
	Crypt  *manage.Secrets
	TRMM   *manage.TRMM
}

// cmdManage is run on the management commands.
func (b *Backup) cmdManage() (err error) {
	log.Debug().
		Msg("Running Manage command")

	bp := manage.BuildParams{
		Filename:  NAME,
		Version:   VERSION,
		BuildTime: BUILD_TIME,
		BuildEnv:  BUILD_ENV,
		GitCommit: GIT_COMMIT,
		GitTag:    GIT_TAG,
		GitBranch: GIT_BRANCH,
		URL:       URL,
		Authors:   AUTHORS,
		License:   LICENSE,
		Copyright: COPYRIGHT,
	}

	basename := strings.TrimSuffix(filepath.Base(b.CLI.Manage.AgentConfig), filepath.Ext(b.CLI.Manage.AgentConfig))
	secretsAction := "save"
	if ufs.Exists(filepath.Join(b.CLI.Manage.ConfigDir, basename+".secrets.hcl")) {
		secretsAction = "load"
	}
	b.Manage = manage.NewManage(manage.Options{
		ConfigDir:     b.CLI.Manage.ConfigDir,
		AgentConfig:   filepath.Join(b.CLI.Manage.ConfigDir, b.CLI.Manage.AgentConfig),
		Secrets:       filepath.Join(b.CLI.Manage.ConfigDir, basename+".secrets.hcl"),
		SecretsAction: secretsAction,
		TRMM:          filepath.Join(b.CLI.Manage.ConfigDir, basename+".trmm.json.base64"),
		BuildParams:   &bp,
	})

	b.Manage.RunManage()
	if b.Manage.HasErrors() {
		log.Error().
			Err(b.Manage.Error()).
			Msg("Failed to run manage command.")
		return b.Manage.Error()
	}

	return nil
}

// cmdFileRepo will print the license information.
func (b *Backup) cmdFileRepo() (err error) {
	log.Debug().
		Msg("Running FileRepo command")

	fr := manage.NewFileRepo(nil).
		SetOptions(&manage.ServerOptions{
			// FIXME: This should not be hard-coded
			Domain:   "download.example.com",
			AuthType: "basic",
		}).
		Search("file")
	if fr.HasErrors() {
		log.Error().
			Err(fr.Error()).
			Str("response", fr.ToString()).
			Msg("Error searching file repository.")
		return fr.Error()
	}
	log.Info().
		Str("response", fr.ToString()).
		Msg("Server response.")

	fr = manage.NewFileRepo(nil).
		SetOptions(&manage.ServerOptions{
			// FIXME: This should not be hard-coded
			Domain:   "download.example.com",
			AuthType: "basic",
			Path:     "trmm",
		}).
		// FIXME: This should not be hard-coded
		Upload("data/a8n-backup_windows_amd64.exe")
	if fr.HasErrors() {
		log.Error().
			Err(fr.Error()).
			Msg("Error uploading file to file repository.")
		return fr.Error()
	}

	log.Info().
		Str("response", fr.ToString()).
		Msg("Server response.")
	return nil
}

// cmdTRMM is run on the agent and will run the backup.
func (b *Backup) cmdTRMM() (err error) {
	log.Debug().
		Msg("Running TRMM command")

	fmt.Println("Command not implemented.")

	return nil
}

// cmdAgentBackup is a stub function for agent commands.
func (b *Backup) cmdAgentBackup() (err error) {
	return nil
}

// cmdAgentRestic is a stub function for agent commands.
func (b *Backup) cmdAgentRestic() (err error) {
	return nil
}
