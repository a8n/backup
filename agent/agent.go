// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// Package agent is the agent that performs the backup on the computers.
package agent

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclparse"
	"github.com/rs/zerolog/log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"time"

	"gitlab.com/a8n/backup/lib"
	"gitlab.com/a8n/backup/lib/crypto"
	ufs "gitlab.com/a8n/backup/lib/fs"
	"gitlab.com/a8n/backup/lib/pkg"
)

// Agent command structure
type Agent struct {
	message  *crypto.Message
	payload  string
	Settings *lib.AgentTemplate
	Restic   restic
	keys     keys
	cmd      string
	err      error `default:"nil"`
}

// keys is the Agent and TRMM encryption keys.
type keys struct {
	TRMM  *crypto.KeyPair
	Agent *crypto.KeyPair
}

type restic struct {
	Version string
	Command []string
	// InstallDir is the installation directory and contains bin for the binaries.
	InstallDir string
	// InstallBin is the full path to the binary to run
	InstallBin string
	// FileExt is the file extension of the binary.
	FileExt string
}

const (
	// backupPublicKey is the name of the environmental variable for the TRMM public key.
	backupPublicKey = "BACKUP_PUBLIC_KEY"
	// backupPayload is the name of the environmental variable for the encrypted settings from TRMM.
	backupPayload = "BACKUP_PAYLOAD"
	// cmdTimeout is the timeout (seconds) for all commands
	cmdTimeout = 300
)

// NewAgent starts a new Manage request
func NewAgent() *Agent {
	return &Agent{
		message: crypto.NewMessage(),
		keys: keys{
			Agent: crypto.NewKeyPair("AgentConfig"),
			TRMM:  crypto.NewKeyPair("TRMM"),
		},
		Settings: &lib.AgentTemplate{},
	}
}

// CmdBackup will run the agent command (`a8n-backup agent run`).
func (a *Agent) CmdBackup(paths []string) *Agent {
	if a.err != nil {
		return a
	}

	if len(paths) == 0 {
		log.Info().
			Msg("Source paths were not specified on the command line. Continuing with source paths in config.")
	}

	if len(paths) > 0 {
		log.Info().
			Msg("Appending source paths to source paths in config.")
		a.Settings.Source.Paths = append(a.Settings.Source.Paths, paths...)
	}

	a.Restic.Command = []string{
		"backup",
	}
	a.init().
		getInstalledVersion().
		exportEnv().
		execRestic()
	if a.HasErrors() {
		log.Error().
			Err(a.err).
			Msg("Failed to run backup command on agent.")
		return a
	}

	return a
}

// CmdRestic will run the restic command (`a8n-backup agent restic ...`).
func (a *Agent) CmdRestic(cmd []string) *Agent {
	if a.err != nil {
		return a
	}

	log.Info().
		Strs("restic_command", cmd).
		Msg("Running Restic command.")
	if len(cmd) == 0 {
		a.err = errors.New("restic command not provided")
		log.Error().
			Err(a.err).
			Strs("restic_command", cmd).
			Msg("Restic command not provided.")
		return a
	}

	switch cmd[0] {
	case "backup":
	case "cache":
	case "cat":
	case "check":
	case "diff":
	case "dump":
	case "find":
	case "forget":
	case "generate":
	case "help":
	case "init":
	case "key":
	case "list":
	case "ls":
	case "migrate":
	case "mount":
	case "prune":
	case "rebuild_index":
	case "recover":
	case "restore":
	case "self_update":
	case "snapshots":
	case "stats":
	case "tag":
	case "unlock":
	case "--verbose":
	default:
		log.Info().
			Strs("restic_command", cmd).
			Msg("Invalid Restic command")
		return a
	}

	a.Restic.Command = cmd
	a.init()
	a.getInstalledVersion()
	a.exportEnv()
	a.execRestic()

	return a
}

// execRestic will run restic
func (a *Agent) execRestic() *Agent {
	if a.err != nil {
		return a
	}

	restic := filepath.Join(a.Restic.InstallBin, "restic"+a.Restic.FileExt)
	if !ufs.Exists(restic) {
		err := pkg.Install("restic", a.Restic.InstallDir)
		if err != nil {
			log.Error().
				Err(err).
				Str("install_dir", a.Restic.InstallDir).
				Str("install_bin", a.Restic.InstallBin).
				Msg("Failed to install the restic binary")
			return a
		}
	}
	log.Debug().
		Str("command", restic).
		Strs("args", a.Restic.Command).
		Msg("Running restic command.")
	var stdout string
	var stderr string
	stdout, stderr = a.exec(restic, a.Restic.Command...)

	if a.err != nil {
		log.Error().
			Err(a.err).
			Str("stdout", stdout).
			Str("stderr", stderr).
			Msg("Failed to run restic command.")
		return a
	}
	if len(stderr) > 0 {
		fmt.Println("==================== Stderr ====================")
		fmt.Println(stderr)
	}
	if len(stdout) > 0 {
		fmt.Println("==================== Output ====================")
		fmt.Println(stdout)
	}

	return a
}

// getInstalledVersion will get the version of Restic that's already installed.
func (a *Agent) getInstalledVersion() *Agent {
	if a.err != nil {
		return a
	}

	// Check if restic is installed.
	restic := filepath.Join(a.Restic.InstallBin, "restic"+a.Restic.FileExt)
	if !ufs.Exists(restic) {
		a.Restic.Version = "v0.0.0"
		log.Info().
			Str("restic", restic).
			Msg("Restic is not installed. Returning version v0.0.0.")
		return a
	}

	args := []string{
		"version",
	}

	log.Debug().
		Str("command", restic).
		Strs("args", args).
		Msg("Running restic command.")
	var stdout string
	var stderr string
	stdout, stderr = a.exec(restic, args...)

	if a.err != nil {
		log.Error().
			Err(a.err).
			Str("stdout", stdout).
			Str("stderr", stderr).
			Msg("Failed to run restic command.")
		return a
	}

	log.Debug().
		Str("version", stdout).
		Msg("Restic version from restic command.")

	re := regexp.MustCompile(`restic ([0-9\.]+)`)
	m := re.FindStringSubmatch(stdout)
	if m == nil {
		log.Info().
			Str("matches", m[0]).
			Msg("Could not extract restic version from `restic version`.")
		return a
	}
	// FIXME: Check if restic needs to be updated.
	a.Restic.Version = m[0]
	log.Info().
		Str("version", a.Restic.Version).
		Msg("Restic version.")

	return a
}

func (a *Agent) exec(command string, args ...string) (stdout string, stderr string) {
	if a.err != nil {
		return "", ""
	}

	if !ufs.Exists(command) {
		a.err = errors.New("command was not found")
		log.Error().
			Err(a.err).
			Str("command", command).
			Msg("Command was not found")
		return "", ""
	}

	ctx, cancel := context.WithTimeout(context.Background(), cmdTimeout*time.Second)
	var errBuffer bytes.Buffer
	var outBuffer bytes.Buffer
	defer cancel()
	cmd := exec.CommandContext(ctx, command, args...)
	cmd.Stderr = &errBuffer
	cmd.Stdout = &outBuffer
	a.err = cmd.Run()
	if a.err != nil {
		if ctx.Err() != nil {
			log.Error().
				Err(ctx.Err()).
				Str("command", command).
				Strs("args", args).
				Msg("Command cancelled due to context.")
			return "", ""
		}
		log.Error().
			Err(a.err).
			Str("command", command).
			Strs("args", args).
			Strs("args", args).
			Str("stderr", errBuffer.String()).
			Str("stdout", outBuffer.String()).
			Msg("Failed to run command.")
		return "", ""
	}

	log.Debug().
		Int("stderr_length", errBuffer.Len()).
		Int("stdout_length", outBuffer.Len()).
		Msg("Command exited successfully.")

	return outBuffer.String(), errBuffer.String()
}

// getPublicKey will get the public key from the environmental variable.
func (a *Agent) getPublicKey() *Agent {
	if a.err != nil {
		return a
	}

	var p crypto.PEMPair
	p.Public = os.Getenv(backupPublicKey)
	p.Private = ""
	a.keys.TRMM.FromPEM(p)

	if a.keys.TRMM.Public.String() == "" {
		log.Warn().
			Str("backupPublicKey", p.Public).
			Str("trmm_public_key", a.keys.TRMM.Public.String()).
			Msg("Failed to decode backupPublicKey from PEM format.")
		return a
	}

	return a
}

// getPrivateKey will get the private key from base64 encoded KEYS compiled into the agent.
func (a *Agent) getPrivateKey() *Agent {
	if a.err != nil {
		return a
	}

	var k []byte
	k, a.err = base64.StdEncoding.DecodeString(KEYS)
	if a.err != nil {
		log.Error().
			Err(a.err).
			Str("keys", KEYS).
			Msg("Failed to base64 decode KEYS.")
		return a
	}

	var pair crypto.PEMPair
	a.err = json.Unmarshal(k, &pair)
	a.keys.Agent.FromPEM(pair)
	if a.err != nil {
		log.Error().
			Err(a.err).
			Str("public_key", a.keys.Agent.Public.String()).
			Str("private_key", a.keys.Agent.Private.String()).
			Msg("Failed to unmarshal JSON public/private Key pair.")
		return a
	}

	return a
}

// getPayload will get the encrypted payload from the environmental variable.
func (a *Agent) getPayload() *Agent {
	if a.err != nil {
		return a
	}

	a.payload = os.Getenv(backupPayload)
	if len(a.payload) == 0 {
		a.err = errors.New(fmt.Sprintf("environmental variable %s not set", backupPayload))
		log.Error().
			Err(a.err).
			Str("payload", a.payload).
			Str("backupPayload", backupPayload).
			Msg("Failed to get backupPayload environmental variable.")
		return a
	}

	return a
}

// decryptPayload will get the encrypted payload from the environmental variable.
func (a *Agent) decryptPayload() *Agent {
	if a.err != nil {
		return a
	}

	log.Info().
		Str("payload", a.payload).
		Msg("Decrypting the payload.")
	// Encrypt: Use recipient (AgentConfig) public key and sender (TRMM) private key
	// Decrypt: Use sender (TRMM) public key and recipient (AgentConfig) private key
	// See manage.go encryptAgentConfig()
	msg := crypto.NewMessage().
		// Use public key of the sender (TRMM)
		SetPublicKey(a.keys.TRMM.Public).
		// Use private key of the recipient (AgentConfig)
		SetPrivateKey(a.keys.Agent.Private).
		SetMessage([]byte(a.payload)).
		DecodeBase64().
		Decrypt(nil)
	if msg.HasErrors() {
		log.Error().
			Err(msg.Error()).
			Msg("Failed to decrypt the message.")
		a.err = msg.Error()
		return a
	}

	/*
	   log.Debug().
	   		Str("m", fmt.Sprintf("%#v", msg)).
	   		Msg("Message struct")
	*/

	if msg.String() == "" {
		a.err = errors.New("decrypted message is empty")
		log.Error().
			Err(a.err).
			Msg("Decrypted message is empty.")
		return a
	}

	parser := hclparse.NewParser()
	var diags hcl.Diagnostics
	var f *hcl.File
	f, diags = parser.ParseHCL([]byte(msg.String()), "agent.hcl")
	if diags.HasErrors() {
		a.err = diags
		log.Error().
			Err(a.err).
			Str("hcl", msg.String()).
			Msg("Failed to parse HCL struct.")
		return a
	}

	decodeDiags := gohcl.DecodeBody(f.Body, nil, a.Settings)
	diags = append(diags, decodeDiags...)
	if diags.HasErrors() {
		a.err = diags
		log.Error().
			Err(a.err).
			Msg("Failed to unmarshal HCL to struct.")
		return a
	}

	return a
}

// processEnv will process the restic settings to prepare them for export to environmental variables.
func (a *Agent) processEnv() *Agent {
	if a.err != nil {
		return a
	}

	// FIXME: Support other providers
	if a.Settings.Storage.Provider != "rest" {
		a.err = errors.New("unsupported storage provider")
		log.Error().
			Str("storage.provider", a.Settings.Storage.Provider).
			Msg("Unsupported storage provider. Only rest is available now.")
		return a
	}

	r := fmt.Sprintf("%s:https://%s:%s@%s:%s/%s",
		a.Settings.Storage.Provider,
		*a.Settings.Storage.Rest.Username,
		*a.Settings.Storage.Rest.Password,
		*a.Settings.Storage.Rest.Host,
		*a.Settings.Storage.Rest.Port,
		*a.Settings.Storage.Rest.Path)
	a.Settings.Restic.Repository = &r
	return a
}

// exportEnv will export the config into environmental variables for restic
func (a *Agent) exportEnv() *Agent {
	if a.err != nil {
		return a
	}

	a.processEnv()
	a.err = os.Setenv("RESTIC_PASSWORD", *a.Settings.Restic.Password)
	if a.err != nil {
		log.Error().
			Err(a.err).
			Str("RESTIC_PASSWORD", *a.Settings.Restic.Password).
			Msg("Failed to set environmental variable.")
		return a
	}

	switch a.Settings.Storage.Provider {
	case "rest":
		a.err = os.Setenv("RESTIC_REPOSITORY", *a.Settings.Restic.Repository)
		if a.err != nil {
			log.Error().
				Err(a.err).
				Str("RESTIC_REPOSITORY", *a.Settings.Restic.Repository).
				Msg("Failed to set environmental variable.")
			return a
		}

	case "backblaze":
		a.err = os.Setenv("B2_ACCOUNT_ID", *a.Settings.Restic.B2AccountID)
		if a.err != nil {
			log.Error().
				Err(a.err).
				Str("B2_ACCOUNT_ID", *a.Settings.Restic.B2AccountID).
				Msg("Failed to set environmental variable.")
			return a
		}

		a.err = os.Setenv("B2_ACCOUNT_KEY", *a.Settings.Restic.B2AccountKey)
		if a.err != nil {
			log.Error().
				Err(a.err).
				Str("B2_ACCOUNT_KEY", *a.Settings.Restic.B2AccountKey).
				Msg("Failed to set environmental variable.")
			return a
		}
	default:
		a.err = errors.New("unsupported storage provider")
		log.Error().
			Err(a.err).
			Str("storage.provider", a.Settings.Storage.Provider).
			Msg("Unsupported storage provider. Only 'rest' is supported.")
		return a
	}

	return a
}

// init will initialize and gather the necessary configs.
func (a *Agent) init() *Agent {
	if a.err != nil {
		return a
	}

	// Determine install location based on OS
	switch runtime.GOOS {
	case "windows":
		fallthrough
	case "windows_nt":
		// FIXME: Use $ENV:ProgramData for internationalization.
		// FIXME: Move this to $ENV:CommonProgramW6432 or $ENV:CommonProgramFiles (Windows 7)
		// $ENV:ProgramData is all users profile and general users have write access.
		// See the following links about developing for Microsoft.
		// https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2010/s2esdf4x(v=vs.100)?redirectedfrom=MSDN
		// https://docs.microsoft.com/en-us/windows/deployment/usmt/usmt-recognized-environment-variables
		a.Restic.InstallDir = `C:\Program Files\Common Files\a8n`
		a.Restic.FileExt = ".exe"
	case "darwin":
		// See the following links about developing for MacOS and the security of downloading software.
		// https://github.com/Homebrew/homebrew-cask/issues/70798
		// https://docs.brew.sh/FAQ#why-cant-i-open-a-mac-app-from-an-unidentified-developer
		// https://github.com/alacritty/alacritty/issues/4673#issuecomment-772893889
		// https://developer.apple.com/support/membership-fee-waiver/
		a.Restic.InstallDir = "/opt/a8n"
		a.Restic.FileExt = ""
	case "aix":
		fallthrough
	case "dragonfly":
		fallthrough
	case "freebsd":
		fallthrough
	case "linux":
		fallthrough
	case "netbsd":
		fallthrough
	case "openbsd":
		fallthrough
	case "sunos":
		a.Restic.InstallDir = "/opt/a8n"
		a.Restic.FileExt = ""
	default:
		log.Warn().
			Str("OS", runtime.GOOS).
			Msg("Failed to determine OS. Using nix style install dir.")
		a.Restic.InstallDir = "/opt/a8n"
		a.Restic.FileExt = ""
	}

	if !ufs.Exists(a.Restic.InstallDir) {
		a.err = os.MkdirAll(a.Restic.InstallDir, os.ModeDir)
		if a.err != nil {
			log.Error().
				Err(a.err).
				Str("install_dir", a.Restic.InstallDir).
				Msg("Failed to create install directory.")
			return a
		}
	}

	a.Restic.InstallBin = filepath.Join(a.Restic.InstallDir, "bin")
	if !ufs.Exists(a.Restic.InstallBin) {
		a.err = os.MkdirAll(a.Restic.InstallBin, os.ModeDir)
		if a.err != nil {
			log.Error().
				Err(a.err).
				Str("install_bin", a.Restic.InstallBin).
				Msg("Failed to create install bin directory.")
			return a
		}
	}

	a.getPayload().
		getPublicKey().
		getPrivateKey().
		decryptPayload()

	if a.err != nil {
		log.Error().
			Err(a.err).
			Msg("Failed to gather and initialize the agent.")
		return a
	}

	log.Info().
		Str("file_ext", a.Restic.FileExt).
		Str("install_dir", a.Restic.InstallDir).
		Str("install_bin", a.Restic.InstallBin).
		Msg("Initializing the agent.")

	return a
}

// Errors returns errors present, or nil otherwise.
func (a *Agent) Error() error {
	return a.err
}

// HasErrors returns true if errors are present.
func (a *Agent) HasErrors() bool {
	return a.err != nil
}

// ErrorMessage returns the error message.
func (a *Agent) ErrorMessage() string {
	return a.err.Error()
}
