// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is included in the Agent build.
//go:build agent

// Package agent is the agent that performs the backup on the computers.
package agent

import _ "embed"

//go:embed agent-keys.json.base64
var KEYS string
