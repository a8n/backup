// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// Package backup integrates with TRMM to provide backups.
package backup

import (
	"bytes"
	"fmt"
	"github.com/rs/zerolog/log"
	"runtime"
	"text/template"
)

// External variables pulled in during the build process
var (
	// NAME is used as the -o target in the build process.
	NAME = "a8n-backup"
	// VERSION TODO: Add API version, GUI version, and possibly other versions.
	VERSION    = "unknown version"
	BUILD_TIME = "unknown time"
	BUILD_ENV  = "unknown env"
	GIT_COMMIT = "unknown git commit"
	GIT_TAG    = "unknown git tag"
	GIT_BRANCH = "unknown git branch"
)

const (
	URL          = "https://gitlab.com/a8n/backup"
	AUTHORS      = "David Randall"
	LICENSE      = "PostgreSQL"
	COPYRIGHT    = "(c) 2022 The a8n Authors"
	ORGANIZATION = "Nice Guy IT, LLC"
	YEAR         = "2022"
)

// NewBackup starts a new backup request.
func NewBackup() *Backup {
	return &Backup{}
}

// GetCLI gets the configuration from the cli and env vars.
func (b *Backup) GetCLI() (err error) {
	b.CLI = NewCLI()
	err = b.CLI.ParseCLI()

	if err != nil {
		log.Error().
			Err(err).
			Err(err).
			Msg("Failed to parse CLI commands.")
		return err
	}

	return nil
}

// Run the command specified on the command line
func (b *Backup) Run() (err error) {

	switch kongContext.Command() {
	case "manage":
		err = b.cmdManage()
		if err != nil {
			log.Error().
				Err(err).
				Err(err).
				Msg("Failed to run manage command.")
			return err
		}

	case "trmm <script>":
		_ = b.cmdTRMM()

	case "agent backup":
		_ = b.cmdAgentBackup()
	case "agent backup <paths>":
		_ = b.cmdAgentBackup()

	case "agent restic <command>":
		_ = b.cmdAgentRestic()

	case "version":
		_ = b.cmdVersion()

	case "license":
		_ = b.cmdLicense()

	case "filerepo":
		// TODO: This is temporary to test the file repo commands.
		_ = b.cmdFileRepo()

	default:
		log.Info().
			Str("command", kongContext.Command()).
			Msg("Backup command")
	}

	return nil
}

// cmdVersion will print the version information.
func (b *Backup) cmdVersion() (err error) {
	log.Debug().
		Msg("Printing version information")
	t := template.Must(template.New("version").Parse(`
Name:       {{.Name}}
Version:    {{.Version}}
Build Env:  {{.BuildEnv}}
Build Time: {{.BuildTime}}
Go Version: {{.GoVersion}}
Go OS/Arch: {{.GoOSArch}}
Git Tag:    {{.GitTag}}
Git Commit: {{.GitCommit}}
Git Branch: {{.GitBranch}}
URL:        {{.URL}}
Authors:    {{.Authors}}
License:    {{.License}}
Copyright:  {{.Copyright}}
`))
	buf := bytes.NewBuffer(nil)
	err = t.Execute(buf, map[string]interface{}{
		"Name":      NAME,
		"Version":   VERSION,
		"BuildTime": BUILD_TIME,
		"BuildEnv":  BUILD_ENV,
		"GoVersion": runtime.Version(),
		"GoOSArch":  runtime.GOOS + "/" + runtime.GOARCH,
		"GitTag":    GIT_TAG,
		"GitCommit": GIT_COMMIT,
		"GitBranch": GIT_BRANCH,
		"URL":       URL,
		"Authors":   AUTHORS,
		"License":   LICENSE,
		"Copyright": COPYRIGHT,
	})
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error executing version template.")
		return err
	}
	fmt.Println(buf.String())
	return nil
}

// cmdLicense will print the license information.
func (b *Backup) cmdLicense() (err error) {
	log.Debug().
		Msg("Printing license information")
	t := template.Must(template.New("version").Parse(`PostgreSQL License

Copyright (c) {{.Year}}, {{.Organization}}

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose, without fee, and without a written
agreement is hereby granted, provided that the above copyright notice
and this paragraph and the following two paragraphs appear in all
copies.

IN NO EVENT SHALL {{.Organization}} BE LIABLE TO ANY PARTY FOR DIRECT,
INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING
LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
DOCUMENTATION, EVEN IF {{.Organization}} HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

{{.Organization}} SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
AND {{.organization}} HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
`))
	buf := bytes.NewBuffer(nil)
	err = t.Execute(buf, map[string]interface{}{
		"Organization": ORGANIZATION,
		"Year":         YEAR,
	})
	if err != nil {
		log.Error().
			Err(err).
			Err(err).
			Msg("Error executing version template.")
		return err
	}
	fmt.Println(buf.String())
	return nil
}
