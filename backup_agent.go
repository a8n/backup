// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is included in the Agent build.
//go:build agent

// Package backup integrates with TRMM to provide backups.
package backup

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/a8n/backup/agent"
)

// Backup is the backup config
type Backup struct {
	CLI *CLI
}

// cmdAgentBackup is run on the agent and will run the backup.
func (b *Backup) cmdAgentBackup() (err error) {
	log.Debug().
		Msg("Running AgentConfig command")

	a := agent.NewAgent().
		CmdBackup(b.CLI.Agent.Backup.Paths)
	if a.HasErrors() {
		log.Error().
			Err(a.Error()).
			Msg("Failed to run agent run command.")
		return a.Error()
	}

	return nil
}

// cmdAgentRestic will run a restic command on the agent.
func (b *Backup) cmdAgentRestic() (err error) {
	log.Debug().
		Msg("Running AgentConfig command")

	a := agent.NewAgent().
		CmdRestic(b.CLI.Agent.Restic.Command)
	if a.HasErrors() {
		log.Error().
			Err(a.Error()).
			Msg("Failed to run restic command on agent.")
		return a.Error()
	}

	return nil
}

// cmdManage is a stub function for manage commands.
func (b *Backup) cmdManage() (err error) {
	return nil
}

// cmdFileRepo is a stub function for manage commands.
func (b *Backup) cmdFileRepo() (err error) {
	return nil
}

// cmdTRMM is a stub function for manage commands.
func (b *Backup) cmdTRMM() (err error) {
	return nil
}
