// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// Package main is the main entry for the 'backup' cli tool.
package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/a8n/backup"
	"os"
)

// Download from GitHub
// https://github.com/google/go-github/blob/7a7a7f0943f3fcffc4b8f30feaaed7333b035a99/github/repos_contents.go
// Also useful
// https://gist.github.com/metal3d/002e4f0d8545f83c2ace
// Just-Install version
// https://github.com/just-install/just-install/blob/19d919b4a2d39cdae13cb9de1f47c0aeced6c2c1/cmd/just-install/action_install.go

// main program
func main() {
	var err error
	bu := backup.NewBackup()
	err = bu.GetCLI()
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to process the CLI commands.")
		os.Exit(1)
	}

	err = bu.Run()
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to run backup command.")
		os.Exit(1)
	}

	os.Exit(0)
}
