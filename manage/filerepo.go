// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is not included in the Agent build.
//go:build !agent

// Package manage provides the management interface for the backup system.
package manage

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/h2non/filetype"
	"github.com/rs/zerolog/log"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"

	"gitlab.com/a8n/backup/lib"
	ufs "gitlab.com/a8n/backup/lib/fs"
)

// FileRepo is the main structure for a file repository.
type FileRepo struct {
	server  *FileServer
	file    *File
	options *FileOptions
	cmd     string
	err     error
}

// FileOptions contains options for the FileRepo
type FileOptions struct {
	Action string
}

// FileServer contains information about the file server.
type FileServer struct {
	url         *url.URL
	method      string
	body        *io.Reader
	Options     *ServerOptions
	Request     *http.Request
	Response    []byte
	contentType string `default:"application/json"`
}

// ServerOptions will set the options for the server.
type ServerOptions struct {
	Domain   string
	Path     string
	FilePath string
	AuthType string // "basic" | "none"
	Username string
	Password string
}

// File contains the information about the file.
type File struct {
	name     string
	dir      string
	fullPath string
	handle   *os.File
	mimeType string `default:"application/json"`
}

// GHSResponse is the response object from GoHttpServer
type GHSResponse struct {
	Auth  GHSAccessConf     `json:"auth"`
	Files []GHSHTTPFileInfo `json:"files"`
}

// GHSAccessConf is the access configuration for GoHttpServer
// https://github.com/codeskyblue/gohttpserver/blob/26545513de66f0cf6975bfad48358586dc413779/httpstaticserver.go#L490
type GHSAccessConf struct {
	Upload       bool             `yaml:"upload" json:"upload"`
	Delete       bool             `yaml:"delete" json:"delete"`
	Users        []GHSUserControl `yaml:"users" json:"users"`
	AccessTables []GHSAccessTable `yaml:"accessTables" json:"AccessTables"`
}

// GHSHTTPFileInfo is the file information for GoHttpServer
// https://github.com/codeskyblue/gohttpserver/blob/26545513de66f0cf6975bfad48358586dc413779/httpstaticserver.go#L469
type GHSHTTPFileInfo struct {
	Name    string `json:"name"`
	Path    string `json:"path"`
	Type    string `json:"type"`
	Size    int64  `json:"size"`
	ModTime int64  `json:"mtime"`
}

// GHSAccessTable is the access permissions for GoHttpServer
// https://github.com/codeskyblue/gohttpserver/blob/26545513de66f0cf6975bfad48358586dc413779/httpstaticserver.go#L469
type GHSAccessTable struct {
	Regex string `json:"Regex"`
	Allow bool   `json:"Allow"`
}

// GHSUserControl is the user information for GoHttpServer
// https://github.com/codeskyblue/gohttpserver/blob/26545513de66f0cf6975bfad48358586dc413779/httpstaticserver.go#L469
type GHSUserControl struct {
	Email  string `json:"Email"`
	Upload bool   `json:"Upload"`
	Delete bool   `json:"Delete"`
	Token  string `json:"Token"`
}

// NewFileRepo starts a new FileRepo request.
func NewFileRepo(options *FileOptions) *FileRepo {
	fr := &FileRepo{
		server:  &FileServer{},
		file:    &File{},
		options: options,
	}
	return fr
}

// SetOptions will set the parameters for the server, like domain, path, and authentication.
// Environmental variables are checked first, then options provided.
func (fr *FileRepo) SetOptions(options *ServerOptions) *FileRepo {
	if fr.err != nil {
		return fr
	}

	// Environmental variables are lower priority (set first then overridden).
	// See https://stackoverflow.com/questions/11077223/what-order-of-reading-configuration-values
	fr.server.Options = &ServerOptions{
		Domain:   os.Getenv("FILEREPO_DOMAIN"),
		Path:     os.Getenv("FILEREPO_PATH"),
		AuthType: os.Getenv("FILEREPO_AUTHTYPE"),
		Username: os.Getenv("FILEREPO_USERNAME"),
		Password: os.Getenv("FILEREPO_PASSWORD"),
	}

	if options != nil {
		// ServerOptions provided override env vars
		if options.Domain != "" {
			fr.server.Options.Domain = options.Domain
		}
		if options.Path != "" {
			fr.server.Options.Path = options.Path
		}
		if options.FilePath != "" {
			fr.server.Options.FilePath = options.FilePath
		}
		if options.AuthType != "" {
			fr.server.Options.AuthType = options.AuthType
		}
		if options.Username != "" {
			fr.server.Options.Username = options.Username
		}
		if options.Password != "" {
			fr.server.Options.Password = options.Password
		}
	}

	fr.server.Options.Path = fmt.Sprintf("%s/%s",
		strings.TrimRight(fr.server.Options.Path, "/"),
		strings.Trim(fr.server.Options.FilePath, "/"))

	if fr.server.Options.AuthType == "" {
		// Default to none if not provided.
		fr.server.Options.AuthType = "none"
	} else if fr.server.Options.AuthType != "basic" && fr.server.Options.AuthType != "none" {
		log.Warn().
			Str("auth_type", fr.server.Options.AuthType).
			Msg("Server AuthType is not 'basic' or 'none'")
		// Default to none if not provided.
		fr.server.Options.AuthType = "none"
	}

	if fr.server.Options.AuthType == "basic" {
		if fr.server.Options.Username == "" {
			log.Warn().
				Str("username", fr.server.Options.Username).
				Msg("Server username is not provided")
		}
		if fr.server.Options.Password == "" {
			log.Warn().
				Msg("Server password is not provided")
		}
	}

	// Domain is required. All others are not.
	if fr.server.Options.Domain == "" {
		fr.err = errors.New("server domain is not provided (empty)")
		log.Error().
			Err(fr.err).
			Str("domain", fr.server.Options.Domain).
			Msg("Server domain is not provided (empty)")
		return fr
	}

	fr.server.url = &url.URL{
		Scheme: "https",
		Host:   strings.TrimRight(fr.server.Options.Domain, "/"),
		Path:   strings.Trim(fr.server.Options.Path, "/"),
	}

	log.Debug().
		Str("domain", fr.server.Options.Domain).
		Str("path", fr.server.Options.Path).
		Str("file_path", fr.server.Options.FilePath).
		Str("auth_type", fr.server.Options.AuthType).
		Str("username", fr.server.Options.Username).
		Msg("File server config.")

	return fr
}

// openFile will open the given file.
func (fr *FileRepo) openFile() *FileRepo {
	if fr.err != nil {
		return fr
	}

	if fr.file.dir == "" {
		// Default is current directory
		fr.file.dir = "."
	}

	file, err := os.Open(fr.file.dir)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("file", fr.file.dir).
			Msg("Error opening file")
		return fr
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Error().
				Err(err).
				Str("file", fr.file.dir).
				Msg("Error closing file")
		}
	}(file)
	fr.file.handle = file
	return fr
}

// openServer will open a request to the HTTP server.
func (fr *FileRepo) openServer() *FileRepo {
	if fr.err != nil {
		return fr
	}

	// See [this post] about redirects
	// [this post]: https://stackoverflow.com/a/31309385
	if fr.server.body == nil {
		fr.server.Request, fr.err = http.NewRequest(fr.server.method, fr.server.url.String(), nil)
	} else {
		fr.server.Request, fr.err = http.NewRequest(fr.server.method, fr.server.url.String(), *fr.server.body)
	}
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Msg("Error creating the client HTTP request")
		return fr
	}

	if fr.server.Options.AuthType == "basic" &&
		fr.server.Options.Username != "" &&
		fr.server.Options.Password != "" {
		fr.server.Request.SetBasicAuth(fr.server.Options.Username, fr.server.Options.Password)
	}
	//writer multipart.Writer
	//fr.server.Request.Header.Add("Content-Type", writer.FormDataContentType())
	fr.server.Request.Header.Add("Content-Type", fr.server.contentType)

	return fr
}

// Download will download a file.
func (fr *FileRepo) Download(src string, dst string) *FileRepo {
	if fr.err != nil {
		return fr
	}

	log.Debug().
		Msg("Executing download command")

	// Determine the full path of dst
	if dst == "" {
		// dst is empty. Use src filename in current directory.
		fr.file.fullPath = path.Clean(filepath.Join(".", filepath.Base(src)))
	} else {
		if path.IsAbs(dst) {
			// dst is absolute
			fr.file.fullPath = path.Clean(dst)
		} else {
			// dst is not absolute. Join it with the current directory
			// Note: Do not use Abs() because that will resolve symlinks in the current directory.
			fr.file.fullPath = path.Clean(filepath.Join(".", dst))
		}
	}

	// Split the full path into file and dir parts
	fr.file.dir, fr.file.name = path.Split(fr.file.fullPath)

	if !ufs.IsDir(fr.file.dir) {
		fr.err = os.MkdirAll(fr.file.dir, os.ModeDir)
		if fr.err != nil {
			log.Error().
				Err(fr.err).
				Str("directory", fr.file.dir).
				Str("destination", dst).
				Msg("Failed to create directory for destination.")
			return fr
		}
	}

	if ufs.IsFile(dst) {
		fr.err = errors.New(fmt.Sprintf("destination file exists: %s", dst))
		log.Error().
			Err(fr.err).
			Str("destination", dst).
			Msg("Not overwriting existing file")
		return fr
	}

	var file *os.File
	file, fr.err = os.Create(dst)
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Str("file", dst).
			Msg("Error creating file")
		return fr
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Error().
				Err(err).
				Str("file", dst).
				Msg("Error closing downloaded file")
		}
	}(file)

	fr.server.url.Path = strings.TrimRight(fr.server.url.Path, "/") + "/" +
		strings.Trim(src, "/")
	log.Debug().
		Str("url", fr.server.url.String()).
		Str("urlPath", fr.server.url.Path).
		Msg("Downloading file from server")

	// See this post about redirects
	// https://stackoverflow.com/a/31309385
	var request *http.Request
	request, fr.err = http.NewRequest("GET", fr.server.url.String(), nil)
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Msg("Error creating the HTTP client")
		return fr
	}

	if fr.server.Options.AuthType == "basic" {
		request.SetBasicAuth(fr.server.Options.Username, fr.server.Options.Password)
	}
	client := &http.Client{}

	var response *http.Response
	response, fr.err = client.Do(request)
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Msg("Error creating the HTTP client")
		return fr
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Error().
				Err(err).
				Msg("Error closing the connection")
		}
	}(response.Body)

	// Check for HTTP 200
	if response.StatusCode != 200 {
		fr.err = errors.New(fmt.Sprintf("bad response from server: %s", response.Status))
		log.Error().
			Err(fr.err).
			Str("http_code", response.Status).
			Int("status_code", response.StatusCode).
			Msg("Invalid server response")
		return fr
	}

	var b int64
	b, fr.err = io.Copy(file, response.Body)
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Msg("Error copying the file")
		return fr
	}
	log.Info().
		Int64("bytes", b).
		Msg("Downloaded file")

	rsp := fmt.Sprintf(`{
		"success": true,
		"bytes": %d,
		"name": "%s"
	}`, b, dst)

	buf, _ := ioutil.ReadFile(dst)

	kind, _ := filetype.Match(buf)
	if kind.MIME.Value == "application/x-executable" {
		log.Info().
			Str("mime_value", kind.MIME.Value).
			Msg("File detected as executable. Setting execute bit")
		fr.err = os.Chmod(dst, 0755)
		if fr.err != nil {
			log.Info().
				Err(fr.err).
				Str("file", dst).
				Msg("Failed to set execute bit on file")
			return fr
		}
	}

	fr.server.Response = []byte(rsp)
	return fr
}

// Upload will upload a file.
// TODO: Add a check to see if the file exists in the repository.
func (fr *FileRepo) Upload(src string) *FileRepo {
	if fr.err != nil {
		return fr
	}

	log.Debug().
		Str("file", src).
		Msg("Executing upload command")

	var file *os.File
	file, fr.err = os.Open(src)
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Str("file", src).
			Msg("Error opening file")
		return fr
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Error().
				Err(err).
				Str("file", src).
				Msg("Error closing file")
		}
	}(file)

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	var part io.Writer
	part, fr.err = writer.CreateFormFile("file", filepath.Base(file.Name()))
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Msg("Failed to create form")
		return fr
	}

	var b int64
	b, fr.err = io.Copy(part, file)
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Msg("Error copying the file")
		return fr
	}
	log.Info().
		Int64("bytes", b).
		Msg("Read file for upload")
	fr.err = writer.Close()
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Msg("Error closing the writer")
		return fr
	}

	// See this post about redirects
	// https://stackoverflow.com/a/31309385
	var request *http.Request
	request, fr.err = http.NewRequest("POST", fr.server.url.String(), body)
	if fr.err != nil {
		log.Fatal().
			Err(fr.err).
			Msg("Error creating the HTTP client")
	}

	if fr.server.Options.AuthType == "basic" {
		log.Debug().
			Msg("Setting BASIC auth")
		request.SetBasicAuth(fr.server.Options.Username, fr.server.Options.Password)
	}
	request.Header.Add("Content-Type", writer.FormDataContentType())
	client := &http.Client{}

	log.Debug().
		Str("url", fr.server.url.String()).
		Msg("Upload URL")

	var response *http.Response
	response, fr.err = client.Do(request)
	if fr.err != nil {
		log.Fatal().
			Err(fr.err).
			Msg("Error creating the HTTP client")
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Error().
				Err(err).
				Msg("Error closing the connection")
		}
	}(response.Body)

	// Check for HTTP 200
	if response.StatusCode != 200 {
		fr.err = errors.New(fmt.Sprintf("bad response from server: %s", response.Status))
		log.Error().
			Err(fr.err).
			Str("http_code", response.Status).
			Int("status_code", response.StatusCode).
			Msg("Invalid server response")
		return fr
	}

	var content []byte
	content, fr.err = ioutil.ReadAll(response.Body)
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Bytes("content", content).
			Msg("Error reading the response")
		return fr
	}

	log.Info().
		Str("content", string(content)).
		Msg("Upload successful")
	fr.server.Response = content
	return fr
}

// Search will search for files.
func (fr *FileRepo) Search(term string) *FileRepo {
	if fr.err != nil {
		return fr
	}

	log.Debug().
		Msg("Executing search command")

	q := fr.server.url.Query()
	q.Set("json", "true")
	//q.Set("search", strings.Join(terms, "%20"))
	q.Set("search", term)
	fr.server.url.RawQuery = q.Encode()

	log.Debug().
		Str("url", fr.server.url.String()).
		Msg("Querying URL")

	fr.server.method = http.MethodGet
	fr.server.contentType = "application/json"
	fr.openServer()

	client := &http.Client{}
	var response *http.Response
	response, fr.err = client.Do(fr.server.Request)
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Msg("Error creating the HTTP client")
		return fr
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Error().
				Err(err).
				Msg("Error closing the connection")
		}
	}(response.Body)

	// Check for HTTP 200
	if response.StatusCode != 200 {
		fr.err = errors.New(fmt.Sprintf("bad response from server: %s", response.Status))
		log.Error().
			Err(fr.err).
			Str("http_code", response.Status).
			Int("status_code", response.StatusCode).
			Msg("Invalid server response")
		return fr
	}

	// TODO: Marshal the JSON response
	var content []byte
	content, fr.err = ioutil.ReadAll(response.Body)
	if fr.err != nil {
		log.Error().
			Err(fr.err).
			Bytes("content", content).
			Msg("Error reading the response")
		return fr
	}

	// Check for HTTP 200
	if response.StatusCode != 200 {
		fr.err = errors.New(fmt.Sprintf("bad response from server: %s", response.Status))
		log.Error().
			Err(fr.err).
			Str("content", string(content)).
			Str("http_code", response.Status).
			Int("status_code", response.StatusCode).
			Msg("Invalid server response")
		return fr
	}

	fr.server.Response = content
	return fr
}

// ToString will convert the response to a string.
func (fr *FileRepo) ToString() string {
	if fr.server == nil || fr.server.Response == nil {
		log.Warn().
			Err(fr.err).
			Msg("FileRepo response is nil. Returning empty string.")
		return ""
	}
	return string(fr.server.Response)
}

// doCmd will process the commands.
func (fr *FileRepo) doCmd() error {
	var rsp []byte
	switch fr.cmd {
	case "upload":
		//rsp = fr.Upload()
	case "download":
		//rsp = fr.Download()
	case "search":
		//rsp = fr.Search()
	default:
		log.Error().
			Msg("Invalid command")
		os.Exit(1)
	}

	if rsp == nil {
		log.Info().
			Msg("The command did not produce any output")
		return nil
	}
	j, err := lib.FormatJSON(rsp)
	if err != nil {
		log.Error().
			Err(err).
			Str("json", j).
			Msg("Error converting the response to JSON")
		return err
	}
	fmt.Println(j)

	return nil
}

// Errors returns errors present, or nil otherwise.
func (fr *FileRepo) Error() error {
	return fr.err
}

// HasErrors returns true if errors are present.
func (fr *FileRepo) HasErrors() bool {
	/*
		log.Debug().
			Err(fr.err).
			Str("err", fmt.Sprintf("%#v", fr.err)).
			Bool("(fr.err != nil)", fr.err != nil).
			Msg("HasError error check.")
	*/
	return fr.err != nil
}

// ErrorMessage returns the error message.
func (fr *FileRepo) ErrorMessage() string {
	return fr.err.Error()
}
