// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is not included in the Agent build.
//go:build !agent

// Package manage provides the interface for the backup system.
package manage

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/bitfield/script"
	"github.com/rs/zerolog/log"
	"gitlab.com/a8n/backup/lib/crypto"
	"os"
	"path/filepath"
	"strings"

	ufs "gitlab.com/a8n/backup/lib/fs"
)

// Manage will start a Manage function chain.
type Manage struct {
	Agent   *Agent
	Secrets *Secrets
	TRMM    *TRMM
	Message *crypto.Message
	options *Options
	err     error `default:"nil"`
}

// Options for the manage command. This is more like a config than options.
type Options struct {
	ConfigDir     string
	AgentConfig   string
	Secrets       string
	SecretsAction string // "load" or "save"
	TRMM          string
	BuildParams   *BuildParams
	AgentParams   *AgentParams
}

// AGENT_KEYS must match the go:embed directive in agent/agent.go
// The path is relative to the git root of this repository.
const AGENT_KEYS = "agent/agent-keys.json.base64"

// NewManage starts a new Manage request
func NewManage(options Options) *Manage {
	m := &Manage{}
	m.Agent = NewAgent()
	m.Secrets = NewSecrets()
	m.TRMM = NewTRMM()
	m.options = &options

	m.LoadTemplates()
	if m.err != nil {
		log.Panic().
			Err(m.err).
			Msg("Failed to load the templates files.")
	}

	return m
}

// RunManage will run the management command (`backup manage ...`).
func (m *Manage) RunManage() *Manage {
	if m.err != nil {
		return m
	}

	log.Info().
		Msg("Running manage command.")

	// loadConfig will also generate the secrets config if needed.
	m.loadConfig()
	if m.err != nil {
		log.Error().
			Err(m.err).
			Msg("Failed to load the configuration files.")
		return m
	}

	// Copy the script information from the AgentConfig to TRMM.
	// Zero out the information in AgentConfig.
	m.TRMM.Template.Hosting = m.Agent.Template.Hosting
	m.Agent.Template.Hosting.Download.URL = nil
	m.Agent.Template.Hosting.Download.BasicUsername = nil
	m.Agent.Template.Hosting.Download.BasicPassword = nil
	m.Agent.Template.Hosting.Download.Filename = nil

	m.encryptAgentConfig()
	if m.HasErrors() {
		log.Error().
			Err(m.err).
			Msg("Failed to encrypt the AgentConfig config.")
		return m
	}

	m.TRMM.Template.CustomFields.BackupSettings = string(m.Agent.bytes)
	m.TRMM.Template.CustomFields.TRMMPublicKey = m.Secrets.TRMM.Public.String()
	log.Debug().
		Str("encrypted_config", m.TRMM.Template.CustomFields.BackupSettings).
		Msg("Message encrypted.")

	// Use lowercase os and arch to prevent match failures due to mixed case.
	*m.TRMM.Template.Hosting.OS = strings.ToLower(*m.TRMM.Template.Hosting.OS)
	*m.TRMM.Template.Hosting.Arch = strings.ToLower(*m.TRMM.Template.Hosting.Arch)

	// Use the build process to find the git root so the embedded file path uses an absolute path, not relative path.
	m.options.BuildParams.OS = *m.TRMM.Template.Hosting.OS
	m.options.BuildParams.Arch = *m.TRMM.Template.Hosting.Arch
	m.options.BuildParams.Filename = filepath.Join(m.options.ConfigDir, m.options.BuildParams.Filename)
	bld := NewBuild(m.options.BuildParams).
		SetAgentParams(&AgentParams{
			PublicKey:  m.Secrets.Agent.Public.String(),
			PrivateKey: m.Secrets.Agent.Private.String(),
		}).
		findGitRoot()

	m.SaveEmbedFile(bld.GitRoot)
	if m.HasErrors() {
		log.Error().
			Err(m.err).
			Msg("Failed to save the agent embedded file.")
		return m
	}

	bld.BuildAgent()
	if bld.err != nil {
		log.Error().
			Err(bld.err).
			Msg("Failed to build the backup agent.")
		return m
	}

	m.RemoveEmbedFile(bld.GitRoot)
	if m.HasErrors() {
		log.Error().
			Err(m.err).
			Msg("Failed to remove the agent embedded file.")
		return m
	}

	// Determine the file path on the server. This is appended to the path in FileRepo.
	filePath := ""
	if len(*m.Agent.Template.Tactical.Client) > 0 {
		filePath = fmt.Sprintf("%s/%s", filePath, strings.Trim(*m.Agent.Template.Tactical.Client, "/"))
	}
	if len(*m.Agent.Template.Tactical.Site) > 0 {
		filePath = fmt.Sprintf("%s/%s", filePath, strings.Trim(*m.Agent.Template.Tactical.Site, "/"))
	}
	if len(*m.Agent.Template.Tactical.Agent) > 0 {
		filePath = fmt.Sprintf("%s/%s", filePath, strings.Trim(*m.Agent.Template.Tactical.Agent, "/"))
	}

	// Add the file path to the Download URL in the TRMM config to be used by the script run on the agent to download
	// the a8n-backup file.
	*m.TRMM.Template.Hosting.Download.URL = fmt.Sprintf("%s/%s", strings.TrimRight(*m.TRMM.Template.Hosting.Download.URL, "/"),
		strings.Trim(filePath, "/"))

	// Dev: Comment this out to disable file upload during development.
	fr := NewFileRepo(nil).
		SetOptions(&ServerOptions{
			Domain:   *m.Agent.Template.Hosting.Upload.Domain,
			Path:     *m.Agent.Template.Hosting.Upload.Path,
			FilePath: filePath,
			AuthType: "basic",
			Username: *m.Agent.Template.Hosting.Upload.BasicUsername,
			Password: *m.Agent.Template.Hosting.Upload.BasicPassword,
		}).
		Upload(bld.params.Filename)
	if fr.HasErrors() {
		log.Error().
			Err(fr.Error()).
			Msg("Error uploading file to file repository.")
		return m
	}

	m.saveConfig()
	if m.HasErrors() {
		log.Error().
			Err(m.err).
			Msg("Failed to save the configuration files.")
		return m
	}

	m.err = os.Remove(bld.params.Filename)
	if m.err != nil {
		log.Error().
			Err(m.Error()).
			Str("filename", bld.params.Filename).
			Msg("Failed to remove agent file.")
		return m
	}

	return m
}

// LoadTemplates will load a blank config for the Agent, Secrets and TRMM configs.
// HCL _can_ preserve the original file structure if hclwrite is used. Currently, that is not the case.
// https://pkg.go.dev/github.com/hashicorp/hcl2/gohcl#EncodeIntoBody
// https://pkg.go.dev/github.com/hashicorp/hcl2/hclwrite#example-package-GenerateFromScratch
func (m *Manage) LoadTemplates() *Manage {
	log.Debug().
		Msg("Loading config templates.")

	m.Agent.LoadTemplate()
	if m.Agent.Error() != nil {
		log.Error().
			Err(m.Agent.err).
			Bool("embed", true).
			Str("filename", "templates/agent.hcl").
			Msg("Failed to load agent template (HCL) from file.")
		m.err = m.Agent.Error()
		return m
	}

	m.Secrets.LoadTemplate()
	if m.Secrets.Error() != nil {
		log.Error().
			Err(m.Secrets.err).
			Bool("embed", true).
			Str("filename", "templates/secrets.hcl").
			Msg("Failed to load secrets template (HCL) from file.")
		m.err = m.Secrets.Error()
		return m
	}

	m.TRMM.LoadTemplate()
	if m.TRMM.Error() != nil {
		log.Error().
			Err(m.TRMM.err).
			Bool("embed", true).
			Str("filename", "templates/trmm.hcl").
			Msg("Failed to load trmm template (HCL) from file.")
		m.err = m.TRMM.Error()
		return m
	}

	return m
}

// loadConfig will load the Agent and Secrets configuration files that were specified on
// the command line.
func (m *Manage) loadConfig() *Manage {
	if m.err != nil {
		return m
	}

	if len(m.options.AgentConfig) > 0 {
		log.Info().
			Str("agent_filename", m.options.AgentConfig).
			Msg("Loading agent config file.")
		m.Agent.Load(m.options.AgentConfig)
		if m.Agent.Error() != nil {
			log.Error().
				Err(m.Agent.err).
				Str("filename", m.options.AgentConfig).
				Msg("Failed to load agent config (HCL) from file.")
			m.err = m.Agent.Error()
			return m
		}
	}

	if len(m.options.SecretsAction) > 0 {
		if m.options.SecretsAction == "load" {
			log.Info().
				Str("secrets_filename", m.options.Secrets).
				Msg("Loading secrets config file.")
			m.Secrets.Load(m.options.Secrets)
			if m.Secrets.Error() != nil {
				log.Error().
					Err(m.Secrets.err).
					Str("filename", m.options.Secrets).
					Msg("Failed to load secrets config (HCL) from file.")
				m.err = m.Secrets.Error()
				return m
			}
		}

		if m.options.SecretsAction == "save" {
			log.Info().
				Str("secrets_filename", m.options.Secrets).
				Msg("Generating secrets config file to save.")
			m.Secrets.GenerateKeys()
			if m.Secrets.Error() != nil {
				log.Error().
					Err(m.Secrets.err).
					Str("secrets_filename", m.options.Secrets).
					Msg("Failed to generate secrets config.")
				m.err = m.Secrets.Error()
				return m
			}
		}
	}

	return m
}

// saveConfig will save the Secrets and TRMM configuration files that were specified on
// the command line.
func (m *Manage) saveConfig() *Manage {
	if m.err != nil {
		return m
	}

	// Save as base64 encoded json for easy assignment to the shell script.
	if len(m.options.TRMM) > 0 {
		m.TRMM.Save(m.options.TRMM, "base64")
		if m.TRMM.Error() != nil {
			log.Error().
				Err(m.TRMM.err).
				Str("filename", m.options.TRMM).
				Msg("Failed to save TRMM config (HCL) to file.")
			m.err = m.TRMM.Error()
			return m
		}
	}

	if len(m.options.Secrets) > 0 {
		m.Secrets.Save(m.options.Secrets, "hcl")
		if m.Secrets.Error() != nil {
			log.Error().
				Err(m.Secrets.err).
				Str("filename", m.options.Secrets).
				Msg("Failed to save secrets config (HCL) to file.")
			m.err = m.Secrets.Error()
			return m
		}
	}

	return nil
}

// encryptAgentConfig encrypts the Agent config file with the TRMM (sender) private key and
// Agent (recipient) public key
// FIXME: This should be moved somewhere more appropriate and return the string.
func (m *Manage) encryptAgentConfig() *Manage {
	if m.err != nil {
		return m
	}

	log.Trace().
		Str("public_key", m.Secrets.Agent.Public.String()).
		Str("private_key", m.Secrets.TRMM.Private.String()).
		Msg("Encryption keys")

	// Encrypt: Use recipient (AgentConfig) public key and sender (TRMM) private key
	// Decrypt: Use sender (TRMM) public key and recipient (AgentConfig) private key
	// See agent.go decryptPayload()
	msg := crypto.NewMessage().
		// Use public key of the recipient (AgentConfig)
		SetPublicKey(m.Secrets.Agent.Public).
		// Use private key of the sender (TRMM)
		SetPrivateKey(m.Secrets.TRMM.Private).
		SetMessage(m.Agent.HCL().Bytes()).
		Encrypt(nil).
		EncodeBase64()
	if msg.HasErrors() {
		log.Error().
			Err(msg.Error()).
			Msg("Failed to encrypt the message.")
		m.err = msg.Error()
		return m
	}

	m.Agent.bytes = []byte(msg.String())
	return m
}

// SaveEmbedFile will save the private key to the embedded file for the agent module.
func (m *Manage) SaveEmbedFile(gitRoot string) *Manage {
	if m.err != nil {
		return m
	}

	agentKeys := filepath.Join(gitRoot, AGENT_KEYS)
	agentPath := filepath.Join(filepath.Dir(agentKeys), "agent.go")
	if !ufs.Exists(agentPath) {
		log.Warn().
			Str("embed_filename", agentKeys).
			Str("agent_path", agentPath).
			Msg("Could not find path to agent module. AgentConfig commands will most likely fail.")
	}

	type AgentKeys struct {
		PublicKey  string `json:"public_key"`
		PrivateKey string `json:"private_key"`
	}
	e := AgentKeys{
		PublicKey:  m.Secrets.Agent.Public.String(),
		PrivateKey: m.Secrets.Agent.Private.String(),
	}
	bytes, err := json.MarshalIndent(e, "", "  ")
	if err != nil {
		log.Error().
			Err(err).
			Str("struct", fmt.Sprintf("%#v", e)).
			Msg("Failed to marshal struct to JSON.")
		m.err = err
		return m
	}
	b64 := base64.StdEncoding.EncodeToString(bytes)

	var written int64
	written, m.err = script.Echo(string(b64)).WriteFile(agentKeys)
	if m.err != nil {
		log.Error().
			Err(m.Error()).
			Str("embed_filename", agentKeys).
			Msg("Error saving the private key to the embed file.")
		return m
	}

	if written == 0 {
		m.err = errors.New("embedded file is 0 bytes")
		log.Error().
			Err(m.Error()).
			Str("embed_filename", agentKeys).
			Int64("bytes_written", written).
			Msg("Wrote 0 bytes to the embed file.")
		return m
	}
	return m
}

// RemoveEmbedFile will save the private key to the embedded file for the agent module.
func (m *Manage) RemoveEmbedFile(gitRoot string) *Manage {
	if m.err != nil {
		return m
	}

	agentKeys := filepath.Join(gitRoot, AGENT_KEYS)
	if !ufs.Exists(agentKeys) {
		m.err = errors.New("embedded file does not exist")
		log.Error().
			Err(m.Error()).
			Str("embed_filename", agentKeys).
			Msg("Embedded file does not exist.")
		return m
	}

	m.err = os.Remove(agentKeys)
	if m.err != nil {
		log.Error().
			Err(m.Error()).
			Str("embed_filename", agentKeys).
			Msg("Failed to remove embed file.")
		return m
	}

	return m
}

// Errors returns errors present, or nil otherwise.
func (m *Manage) Error() error {
	return m.err
}

// HasErrors returns true if errors are present.
func (m *Manage) HasErrors() bool {
	return m.err != nil
}

// ErrorMessage returns the error message.
func (m *Manage) ErrorMessage() string {
	return m.err.Error()
}
