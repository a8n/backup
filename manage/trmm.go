// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is not included in the Agent build.
//go:build !agent

// Package manage provides the management interface for the backup system.
package manage

import (
	_ "embed"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/a8n/backup/lib"

	"github.com/bitfield/script"
	"github.com/go-yaml/yaml"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclparse"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/rs/zerolog/log"
)

// TRMM starts a new TRMM function chain
type TRMM struct {
	Template Template
	err      error
	bytes    []byte
}

// Template manages the config for the Tactical RMM integration.
type Template struct {
	CustomFields CustomFields `hcl:"custom_fields,block" json:"custom_fields"`
	Hosting      lib.Hosting  `hcl:"hosting,block" json:"hosting"`
}

type CustomFields struct {
	BackupSettings string `hcl:"a8n_backup_backup_settings,attr" json:"encrypted"`
	TRMMPublicKey  string `hcl:"a8n_backup_trmm_public_key,attr" json:"public_key"`
}

// trmmTemplate is the default template for trmm.hcl.
//go:embed templates/trmm.hcl
var trmmTemplate []byte

// NewTRMM starts a new TRMM request
func NewTRMM() *TRMM {
	return &TRMM{}
}

// LoadTemplate will load the template trmm.hcl embedded into the binary.
func (t *TRMM) LoadTemplate() *TRMM {
	if t.err != nil {
		return t
	}

	log.Debug().
		Int("size", len(trmmTemplate)).
		Msg("Loading trmm template.")

	parser := hclparse.NewParser()
	var diags hcl.Diagnostics
	var f *hcl.File
	f, diags = parser.ParseHCL(trmmTemplate, "trmm.hcl")
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Msg("Failed to parse HCL file.")
		t.err = diags
		return t
	}

	decodeDiags := gohcl.DecodeBody(f.Body, nil, &t.Template)
	diags = append(diags, decodeDiags...)
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Msg("Failed to decode HCL file.")
		t.err = diags
		return t
	}

	return t
}

// Load and parse the TRMM HCL file.
func (t *TRMM) Load(filename string) *TRMM {
	if t.err != nil {
		return t
	}

	if len(filename) == 0 {
		log.Error().
			Msg("TRMM template file is not specified.")
		t.err = errors.New("trmm template file not specified")
		return t
	}

	var diags hcl.Diagnostics
	parser := hclparse.NewParser()
	file, parseDiags := parser.ParseHCLFile(filename)
	diags = append(diags, parseDiags...)
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Str("filename", filename).
			Msg("Failed to parse TRMM HCL file.")
		t.err = diags
		return t
	}

	decodeDiags := gohcl.DecodeBody(file.Body, nil, &t.Template)
	diags = append(diags, decodeDiags...)
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Msg("Failed to decode HCL file.")
		t.err = diags
		return t
	}
	log.Trace().
		Str("filename", filename).
		Str("template", fmt.Sprintf("%#v", t.Template)).
		Msg("Loaded TRMM config.")

	return t
}

// Save the TRMM HCL to a file.
func (t *TRMM) Save(filename string, format string) *TRMM {
	if t.err != nil {
		return t
	}

	var contents string
	var err error
	switch format {
	case "base64":
		contents = t.JSON().encodeBase64()

	case "json":
		contents = t.JSON().String()

	case "hcl":
		contents = t.HCL().String()

	default:
		contents = t.HCL().String()
	}

	if t.err != nil {
		log.Error().
			Err(err).
			Msg("Failed to marshal CryptHCL contents.")
		return t
	}

	if len(contents) == 0 {
		log.Error().
			Err(err).
			Msg("Converting CryptHCL to string resulted in zero length.")
		t.err = errors.New("converting CryptHCL to string resulted in zero length")
		return t
	}

	var b int64
	b, err = script.Echo(contents).WriteFile(filename)
	if err != nil {
		log.Error().
			Err(err).
			Int64("bytes_written", b).
			Str("filename", filename).
			Str("format", format).
			Msg("Failed to write TRMM contents to file.")
		t.err = err
		return t
	}
	if b == 0 {
		log.Error().
			Int64("bytes_written", b).
			Str("filename", filename).
			Str("format", format).
			Msg("File is 0 bytes.")
		t.err = err
		return t
	}

	return t
}

// String will convert the TRMM structure to a string.
func (t *TRMM) String() string {
	if t.err != nil {
		return ""
	}

	// FIXME: This should return the actual bytes.
	if t.err != nil {
		return ""
	}

	return string(t.bytes)
}

// Bytes will convert the TRMM structure to a []byte slice.
func (t *TRMM) Bytes() []byte {
	if t.err != nil {
		return nil
	}

	if len(t.bytes) == 0 {
		// Default is HCL format
		t.HCL()
		if t.err != nil {
			return nil
		}
		return t.bytes
	}

	return t.bytes
}

// HCL will convert the TRMM structure to HCL.
func (t *TRMM) HCL() *TRMM {
	if t.err != nil {
		return t
	}

	f := hclwrite.NewEmptyFile()
	gohcl.EncodeIntoBody(t.Template, f.Body())
	t.bytes = f.Bytes()
	return t
}

// JSON will convert the TRMM structure to JSON.
func (t *TRMM) JSON() *TRMM {
	if t.err != nil {
		return t
	}

	var err error
	t.bytes, err = json.MarshalIndent(t.Template, "", "  ")
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to marshal JSON.")
		t.err = err
		return t
	}

	return t
}

// YAML will convert the TRMM structure to YAML.
func (t *TRMM) YAML() *TRMM {
	if t.err != nil {
		return t
	}

	var err error
	t.bytes, err = yaml.Marshal(t.Template)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to marshal YAML.")
		t.err = err
		return t
	}

	return t
}

// encodeBase64 will encode the bytes to base64.
func (t *TRMM) encodeBase64() string {
	if t.err != nil {
		return ""
	}

	return base64.StdEncoding.EncodeToString(t.bytes)
}

// Errors returns errors present, or nil otherwise.
func (t *TRMM) Error() error {
	return t.err
}

// HasErrors returns true if errors are present.
func (t *TRMM) HasErrors() bool {
	return t.err != nil
}

// ErrorMessage returns the error message.
func (t *TRMM) ErrorMessage() string {
	return t.err.Error()
}
