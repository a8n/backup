#!/usr/bin/env bash
# Copyright 2022, The a8n Authors. All rights reserved.
# SPDX-License-Identifier: PostgreSQL

# Check for the necessary tools before continuing.
if ! which jq >/dev/null; then
    echo "jq is not installed"
    exit 1
fi

if ! which uname >/dev/null; then
    echo "uname is not installed"
    exit 1
fi

if ! which base64 >/dev/null; then
    echo "base64 is not installed"
    exit 1
fi

if [[ $# -eq 0 ]]; then
    echo "No arguments given. The first argument needs to be the base64 encoded version of the TRMM JSON."
    exit 1
fi

# Install directory: /opt/a8n
# FIXME: Download to $tmp and copy to save_location
install_bin="/opt/a8n/bin"

if [[ ! -d "$install_bin" ]]; then
    echo "Save location does not exist. Creating directory: ${install_bin}"
    mkdir --parents "${install_bin}"
fi


# eval gets rid of the single quotes inserted by TRMM.
trmm=$(eval echo $1 | base64 --decode -)
# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
    echo "Could not base64 decode first argument."
    echo "Argument 1:"
    echo "$1"
    exit 1
fi
shift

# Extract the file hosting information.
url=$(echo "${trmm}" | jq -r '. .hosting.download.url')
username=$(echo "${trmm}" | jq -r '. .hosting.download.basic_username')
password=$(echo "${trmm}" | jq -r '. .hosting.download.basic_password')
filename=$(echo "${trmm}" | jq -r '. .hosting.download.filename')
os=$(echo "${trmm}" | jq -r '. .hosting.os')
arch=$(echo "${trmm}" | jq -r '. .hosting.arch')

# Strip trailing slashes from the URL. Double slashes causes problems with gohttpserver.
url=${url%%/}

if [[ -z "$url" ]]; then
    echo "Could not extract URL from settings."
    echo "URL: ${url}"
    exit 1
fi
if [[ -z "$username" ]]; then
    echo "Could not extract username from settings."
    echo "Username: ${username}"
    exit 1
fi
if [[ -z "$password" ]]; then
    echo "Could not extract password from settings."
    exit 1
fi

if [[ -z "$filename" ]]; then
    echo "Could not extract filename from settings."
    echo "Filename: ${filename}"
    exit 1
fi
if [[ -z "$os" ]]; then
    echo "Could not extract os from settings."
    echo "OS: ${os}"
    exit 1
fi
if [[ -z "$arch" ]]; then
    echo "Could not extract arch from settings."
    echo "Arch: ${arch}"
    exit 1
fi

# Use uname to get the os and arch. Wikipedia has a list of `uname -s` values.
# https://en.wikipedia.org/wiki/Uname
# https://stackoverflow.com/questions/394230/how-to-detect-the-os-from-a-bash-script
#
# `uname -m` can be used to determine the architecture.
actual_os=$(uname -s | tr '[A-Z]' '[a-z]')
actual_arch=$(uname -m | tr '[A-Z]' '[a-z]')

if [[ "$os" != "$actual_os" ]]; then
    echo "Settings OS is different from actual OS. Are you using the right settings?"
    echo "Settings OS: ${os}"
    echo "Actual OS: ${actual_os}"
    exit 1
fi
if [[ "$arch" != "$actual_arch" ]]; then
    echo "Settings arch is different from actual arch. Are you using the right settings?"
    echo "Settings arch: ${arch}"
    echo "Actual arch: ${actual_arch}"
    exit 1
fi

filename="${filename}_${os}_${arch}"
echo "Searching for file '${filename}'"

# Check to see if the file exists
response=$(curl --silent --basic --user "${username}:${password}" \
    --output - \
    "${url}?search=${filename}&json=true" | jq -r '. .files[] .name')
if [[ "${response}" != "${filename}" ]]; then
    echo "File does not exist on the web server"
    echo "URL: ${url}?search=${filename}&json=true"
    echo "Filename: ${filename}"
    echo "Files found on server:"
    echo "${response}"
    exit 1
fi

# Download the file
curl --silent --basic --user "${username}:${password}" \
    --output "${install_bin}/${filename}" \
    "${url}/${filename}"
chmod ug+x "${install_bin}/${filename}"

# Variables to pass to the backup program.
BACKUP_PUBLIC_KEY=$(echo "${trmm}" | jq -r '. .custom_fields.public_key')
BACKUP_PAYLOAD=$(echo "${trmm}" | jq -r '. .custom_fields.encrypted')

if [[ -z "$BACKUP_PUBLIC_KEY" ]]; then
    echo "Could not extract public key from settings."
    echo "BACKUP_PUBLIC_KEY: ${BACKUP_PUBLIC_KEY}"
    exit 1
fi

if [[ -z "$BACKUP_PAYLOAD" ]]; then
    echo "Could not extract backup payload from settings."
    echo "BACKUP_PAYLOAD: ${BACKUP_PAYLOAD}"
    exit 1
fi

export BACKUP_PUBLIC_KEY
export BACKUP_PAYLOAD

# The -- at the beginning is to prevent the rest of the arguments from being interpreted as arguments to a8n-backup
# instead of restic.
#"${location}/${filename}" --log-level debug agent -- "$@" >>/var/log/a8n-backup.log 2>&1
"${install_bin}/${filename}" --log-level trace agent -- "$@"
