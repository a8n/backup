<#
	Copyright 2022, The a8n Authors. All rights reserved.
	SPDX-License-Identifier: PostgreSQL

	.Synopsis
    	Backup automation.

	.DESCRIPTION
    	a8n-backup is a wrapper around Restic to perform backups.

#>

param(
	[string] $trmm
)

# Alternative: https://gist.github.com/markwragg/f3ac7bd6098dec485e8f11c1fd056fad
function Write-Timestamp() {
	Param (
		$message
	)
	Write-Output "$( Get-Date -UFormat "%Y-%m-%d %H:%M:%S" ) ${message}"
}

if (! $trmm) {
	Write-Timestamp "No arguments given. The first argument needs to be the base64 encoded version of the TRMM JSON."
	Exit(1)
}

# Install directory: C:\Program Files\Common Files\a8n
# FIXME: Download to %tmp% and copy to $install_bin
if (! $ENV:CommonProgramW6432) {
	# Older windows. Maybe Windows 7?
	$install_base = "${ENV:CommonProgramFiles}"
	$install_dir = "${ENV:CommonProgramFiles}\a8n"
	$install_bin = "${ENV:CommonProgramFiles}\a8n\bin"
} else {
	$install_base = "${ENV:CommonProgramW6432}"
	$install_dir = "${ENV:CommonProgramW6432}\a8n"
	$install_bin = "${ENV:CommonProgramW6432}\a8n\bin"
}

if (-Not (Test-Path -Path $install_dir)) {
	Write-Timestamp "Install bin does not exist. Creating directory: ${install_dir}"
	$null = New-Item -Path $install_base -Name "a8n" -ItemType "directory"
}
if (-Not (Test-Path -Path $install_bin)) {
	Write-Timestamp "Install bin does not exist. Creating directory: ${install_bin}"
	$null = New-Item -Path $install_dir -Name "bin" -ItemType "directory"
}

# TODO: Might need to deal with removing the single quotes added by TRMM.
$tj = [Text.Encoding]::Utf8.GetString([Convert]::FromBase64String($trmm))
if (! $?) {
	Write-Timestamp "Could not base64 decode first argument: $($error[0].ToString() )"
	Write-Timestamp "Stacktrace: $($error[0].Exception.ToString() )"
	Exit(1)
}

# Extract the file hosting information.
[string]$url = (ConvertFrom-Json $tj).hosting.download.url
[string]$username = (ConvertFrom-Json $tj).hosting.download.basic_username
[string]$password = (ConvertFrom-Json $tj).hosting.download.basic_password
[string]$filename = (ConvertFrom-Json $tj).hosting.download.filename
[string]$os = (ConvertFrom-Json $tj).hosting.os
[string]$arch = (ConvertFrom-Json $tj).hosting.arch

# Strip trailing slashes from the URL. Double slashes causes problems with gohttpserver.
$url = $url.Trim('/')

if (! $url) {
	Write-Timestamp "Could not extract URL from settings."
	Write-Timestamp "URL: ${url}"
	Exit(1)
}
if (! $username) {
	Write-Timestamp "Could not extract username from settings."
	Write-Timestamp "Username: ${username}"
	Exit(1)
}
if (! $password) {
	Write-Timestamp "Could not extract password from settings."
	Write-Timestamp "Password: ${password}"
	Exit(1)
}
if (! $filename) {
	Write-Timestamp "Could not extract filename from settings."
	Write-Timestamp "Filename: ${filename}"
	Exit(1)
}
if (! $os) {
	Write-Timestamp "Could not extract os from settings."
	Write-Timestamp "OS: ${os}"
	Exit(1)
}
if (! $arch) {
	Write-Timestamp "Could not extract arch from settings."
	Write-Timestamp "Arch: ${arch}"
	Exit(1)
}

# Even though Windows and the web is case-insensitive, the underlying Linux filesystem is not.
# Lowercase the os and arch to match the filename on the server.
$actual_arch = ($Env:PROCESSOR_ARCHITECTURE).ToLower()
$actual_os = ($Env:OS).ToLower()

if ($os -ne $actual_os) {
	Write-Timestamp "Settings OS is different from actual OS. Are you using the right settings?"
	Write-Timestamp "Settings OS: ${os}"
	Write-Timestamp "Actual OS: ${actual_os}"
	Exit(1)
}
if ($arch -ne $actual_arch) {
	Write-Timestamp "Settings arch is different from actual arch. Are you using the right settings?"
	Write-Timestamp "Settings arch: ${arch}"
	Write-Timestamp "Actual arch: ${actual_arch}"
	Exit(1)
}

if ([Environment]::OSVersion.Version -le (new-object 'Version' 7, 0)) {
	# This is required for Windows 7, 8.1
	Write-Timestamp "Adjusting TLS version(s) for Windows prior to Win 10"
	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls -bor [Net.SecurityProtocolType]::Tls11 -bor [Net.SecurityProtocolType]::Tls12
}

# Prefer stack trace with friendly error message over a cryptic error message.
$ErrorActionPreference = 'SilentlyContinue'

$filename = "${filename}_${os}_${arch}.exe"
Write-Timestamp "Searching for file '${filename}'"

$secure_password = ConvertTo-SecureString -Force -AsPlainText $password
$credentials = New-Object System.Management.Automation.PSCredential($username, $secure_password)
$response = Invoke-RestMethod -Uri "${url}/?search=${filename}&json=true" -Credential $credentials
if (! $?) {
	Write-Timestamp "Failed to search for files on web server: $($error[0].ToString() )"
	Write-Timestamp "Stacktrace: $($error[0].Exception.ToString() )"
	Exit(1)
}

if (! $response) {
	Write-Timestamp "Response from the server is null."
	Write-Timestamp "URL: ${url}/?search=${filename}&json=true"
	Write-Timestamp "Response:", ${response}
	Exit(1)
}

$files = $response | ForEach-Object { $_.files.name }
if (! ($files -contains $filename)) {
	Write-Timestamp "File does not exist on the web server."
	Write-Timestamp "URL: ${url}/?search=${filename}&json=true"
	Write-Timestamp "Searching for filename: ${filename}"
	Write-Timestamp "Server responded with:", $files
	Exit(1)
}

Write-Timestamp "Found file '${filename}'"
Write-Timestamp "Downloading: ${url}/${filename}"
$ProgressPreference = 'SilentlyContinue'
$wc = New-Object net.webclient
$wc.Credentials = $credentials
$wc.DownloadFile("${url}/${filename}", "${install_bin}\${filename}")
if (! $?) {
	Write-Timestamp "Failed to download file from web server: $($error[0].ToString() )"
	Write-Timestamp "Stacktrace: $($error[0].Exception.ToString() )"
	Exit(1)
}

Write-Timestamp "Download complete"
$ENV:BACKUP_PUBLIC_KEY = [string](ConvertFrom-Json $tj).custom_fields.public_key
$ENV:BACKUP_PAYLOAD = [string](ConvertFrom-Json $tj).custom_fields.encrypted

if (! $ENV:BACKUP_PUBLIC_KEY) {
	Write-Timestamp "Could not extract public key from settings."
	Write-Timestamp "BACKUP_PUBLIC_KEY: ${BACKUP_PUBLIC_KEY}"
	Exit(1)
}
if (! $ENV:BACKUP_PAYLOAD) {
	Write-Timestamp "Could not extract backup payload from settings."
	Write-Timestamp "BACKUP_PAYLOAD: ${BACKUP_PAYLOAD}"
	Exit(1)
}

Write-Timestamp "Running a8n-backup"
& $install_bin\$filename --log-level debug agent $args
Write-Timestamp "Finished running a8n-backup"