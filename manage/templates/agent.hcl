# Copyright 2022, The a8n Authors. All rights reserved.
# SPDX-License-Identifier: PostgreSQL

/********************************************************************************
 * Configuration file for the agent.
 *******************************************************************************/

# This file contains the configuration for the backups on one agent.
#
# Keep this in a safe and secure place, such as a password manager or
# an encrypted directory.

# Sources to backup
source {
	paths = [
		".."
	]
}

# Storage holds the restic repository (i.e. backup)
storage {
	provider = "rest"
	rest {
		host     = "restic.example.com"
		port     = "443"
		username = "username"
		password = "some-secret-password"
		path     = "client-site-agent"
	}
}

# Restic options go here
restic {
	password = "restic-secret-password"
	bw_limit = ""
}

# This is for your reference only to note which client/site/agent this backup configuration applies to.
# The file server uses this as a path separator for the agents. These are appended to upload.path below.
# This table uses '/trmm' as an example for path. Notice how if the client is not specified and site is, then the
# site acts like the agent. For this reason it's advisable to always specify the left-most qualifier.
# If you specify site, enter the client.
# If you specify the agent, enter the client and site.
#   | client    | site    | agent    | URL                                                     |
#   |-----------|---------|----------|---------------------------------------------------------|
#   | client-01 |         |          | /trmm/client-01/a8n-backup_{os}_{arch}                  |
#   | client-01 | site-01 |          | /trmm/client-01/site-01/a8n-backup_{os}_{arch}          |
#   | client-01 | site-01 | agent-01 | /trmm/client-01/site-01/agent-01/a8n-backup_{os}_{arch} |
#   |           | site-01 | agent-01 | /trmm/site-01/agent-01/a8n-backup_{os}_{arch}           |
#   |           |         | agent-01 | /trmm/agent-01/a8n-backup_{os}_{arch}                   |
#   |           | site-01 |          | /trmm/site-01/a8n-backup_{os}_{arch}                    |
tactical {
	client = ""
	site   = ""
	agent  = ""
}

# File hosting repository options
hosting {
	# File hosting repository to upload the files. Usually this is the same as the the download options but does not
	# have to be.
	# TODO: Add option to use URL instead of domain/path.
	# Environmental variables (FILEREPO_*) may also be used and will have lower priority.
	upload {
		domain         = "download.example.com" // FILEREPO_DOMAIN
		path           = "/trmm"                // FILEREPO_PATH
		// FIXME: auth_type is not hard coded.
		// auth_type = "basic"                  // FILEREPO_AUTHTYPE
		basic_username = "username"             // FILEREPO_USERNAME
		basic_password = "password"             // FILEREPO_PASSWORD
		# The actual filename will be {filename}_{GOOS}_{GOARCH}[.ext]
		filename       = "a8n-backup"
	}

	# Where should the TRMM script download the agent? These variables are output in the trmm.json file.
	# The contents of these values are not stored in the encrypted version.
	download {
		url            = "https://download.example.com/trmm/"
		basic_username = "username"
		basic_password = "password"
		filename       = "a8n-backup"
	}

	# Windows: "Windows" | "Windows_NT"
	# All others: `uname -s`
	os   = ""
	# Windows: "amd64" | "386" | "arm" | "arm64"
	# All others: `uname -m`
	arch = ""
}
