# Copyright 2022, The a8n Authors. All rights reserved.
# SPDX-License-Identifier: PostgreSQL

/********************************************************************************
 * Tactical RMM configuration file.
 *******************************************************************************/

# This file contains the configuration that is added to TRMM, and is passed
# to the backup agent to run the backup.

# Add these fields to Tactical's custom fields.
custom_fields {
	# Encrypted backup
	a8n_backup_backup_settings = "backup settings"

	# TRMM public key
	a8n_backup_trmm_public_key = "public key"
}

# Where should the script download the agent?
# These are placeholders for the data copied from agent.hcl.
hosting {
	upload {
		domain         = ""
		path           = ""
		basic_username = ""
		basic_password = ""
		# Filename is the base filename without the os and arch.
		# The actual filename will be {filename}_{GOOS}_{GOARCH}[.ext]
		filename       = ""
	}
	download {
		url            = ""
		basic_username = ""
		basic_password = ""
		# Filename is the base filename without the os and arch.
		# The actual filename will be {filename}_{GOOS}_{GOARCH}[.ext]
		filename       = ""
	}
	os   = ""
	arch = ""
}
