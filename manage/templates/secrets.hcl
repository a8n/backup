# Copyright 2022, The a8n Authors. All rights reserved.
# SPDX-License-Identifier: PostgreSQL

/********************************************************************************
 * Encryption keys.
 *******************************************************************************/

# This file contains the encryption keys used to encrypt and decrypt the
# configuration files.
#
# These keys are disposable as they can be (not by default) regenerated
# during the backup agent deployment. To rotate the keys, generate new keys and
# re-encrypt the configuration. A new backup agent is created with the private key.
# TRMM's public key is added to TRMM.
#
# Backup agent public/private keys
# The private key is compiled into the agent.
# The public key is used to encrypt the backup config.
agent {
	public_key  = ""
	private_key = ""
}

# TRMM public/private keys
# The private key is used to encrypt the backup config.
# The public key is added to TRMM. See `trmm.hcl`.
trmm {
	public_key  = ""
	private_key = ""
}
