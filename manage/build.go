// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is not included in the Agent build.
//go:build !agent

// Package manage provides the management interface for the backup system.
package manage

import (
	"errors"
	"fmt"
	"github.com/bitfield/script"
	"github.com/go-git/go-git/v5"
	"github.com/rs/zerolog/log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	utils "gitlab.com/a8n/backup/lib"
	ufs "gitlab.com/a8n/backup/lib/fs"
)

// Build parameters for a target device.
type Build struct {
	params      *BuildParams
	AgentParams *AgentParams
	GitRoot     string
	err         error `default:"nil"`
	bytes       []byte
}

// BuildParams defines the parameters used during building. Some of these options will not be set when using "go build"
// to build the 'backup' cli tool. Most of these should be set when building the 'a8n-backup' agent that is run on the
// endpoints.
type BuildParams struct {
	// OS is the output of uname -s for *nix and
	//       Windows            |     *nix
	// "Windows" | "Windows_NT" | `uname -s`
	OS string
	// Arch is the output of uname -m for *nix and
	// "amd64" | "386" | "arm" | "arm64" | `uname -m`
	Arch      string
	Filename  string
	Version   string
	BuildTime string
	BuildEnv  string
	GitCommit string
	GitTag    string
	GitBranch string
	URL       string
	Authors   string
	License   string
	Copyright string
}

// AgentParams defines the parameters used by the agent, namely the private key. The public key is included to
// output the hash for comparison purposes. This may help identify the agent.
type AgentParams struct {
	PublicKey  string
	PrivateKey string
}

// modulePrefix is the module prefix when defining the parameters for ldflags. See the Stack Overflow discussion on
// [How to set package variable using -ldflags -X in Golang build].
// [How to set package variable using -ldflags -X in Golang build]: https://stackoverflow.com/a/47510909
var modulePrefix = "gitlab.com/a8n/backup"

// NewBuild starts a new backup request.
func NewBuild(params *BuildParams) *Build {
	b := &Build{
		params: params,
	}
	return b
}

// unameMToGOARCH maps `uname -m` to the Go architecture (GOARCH). See [mutagen agent probe.go]
// Since the OS and Arch are entered in the settings, lowercase names are used to prevent human errors.
// [mutagen agent probe.go]: https://github.com/mutagen-io/mutagen/blob/master/pkg/agent/probe.go
func unameMToGOARCH(uname string) (goArch string, err error) {
	unameM := map[string]string{
		// `uname -m`:  GOARCH
		"i386":     "386",
		"i486":     "386",
		"i586":     "386",
		"i686":     "386",
		"x86_64":   "amd64",
		"amd64":    "amd64",
		"armv5l":   "arm",
		"armv6l":   "arm",
		"armv7l":   "arm",
		"armv8l":   "arm64",
		"aarch64":  "arm64",
		"arm64":    "arm64",
		"mips":     "mips",
		"mipsel":   "mipsle",
		"mips64":   "mips64",
		"mips64el": "mips64le",
		"ppc64":    "ppc64",
		"ppc64le":  "ppc64le",
		"riscv64":  "riscv64",
		"s390x":    "s390x",
		// Windows, 32-bit; 64-bit is listed above
		"386": "386",
		"arm": "arm",
	}

	goArch = unameM[uname]
	if goArch == "" {
		log.Error().
			Str("uname_m", uname).
			Msg("Failed to map `uname -m` to GOARCH.")
		return "", errors.New("unknown uname: failed to map uname -m to GOARCH")
	}

	return goArch, nil
}

// unameSToGOOS maps `uname -s` to the Go OS (GOOS). See [mutagen agent probe.go]
// Since the OS and Arch are entered in the settings, lowercase names are used to prevent human errors.
// [mutagen agent probe.go]: https://github.com/mutagen-io/mutagen/blob/master/pkg/agent/probe.go
func unameSToGOOS(uname string) (goOS string, err error) {
	unameS := map[string]string{
		// `uname -S`:  GOOS
		"aix":       "aix",       // AIX
		"darwin":    "darwin",    // Darwin
		"dragonfly": "dragonfly", // DragonFly
		"freebsd":   "freebsd",   // FreeBSD
		"linux":     "linux",     // Linux
		"netbsd":    "netbsd",    // NetBSD
		"openbsd":   "openbsd",   // OpenBSD
		"sunos":     "solaris",   // SunOS
		// Windows
		"windows":    "windows", // Windows
		"windows_nt": "windows", // Windows_NT
	}

	goOS = unameS[strings.ToLower(uname)]
	if goOS == "" {
		log.Error().
			Str("uname_m", uname).
			Msg("Failed to map `uname -s` to GOOS.")
		return "", errors.New("unknown uname: failed to map uname -s to GOOS")
	}

	return goOS, nil
}

// SetOS will set the OS for the target build.
func (b *Build) SetOS(o *string) *Build {
	if b.err != nil {
		return b
	}

	if o != nil {
		b.params.OS = *o
	}

	if b.params.OS == "" {
		b.err = errors.New("os is not set")
		log.Error().
			Err(b.err).
			Str("os", b.params.OS).
			Msg("OS is not set. Build failed.")
		return b
	}

	// Map `uname -s` to GOOS
	var goos string
	goos, b.err = unameSToGOOS(b.params.OS)
	if b.err != nil {
		log.Error().
			Str("uname_s", b.params.OS).
			Str("goos", goos).
			Msg("Failed to map uname -s to GOOS.")
		return b
	}

	env := "GOOS"
	b.err = os.Setenv(env, goos)
	if b.err != nil {
		log.Error().
			Err(b.err).
			Str("env", env).
			Str("value", goos).
			Msg("Failed to set the GOOS environmental variable.")
		return b
	}

	return b
}

// SetArch will set the Arch for the target build.
func (b *Build) SetArch(arch *string) *Build {
	if b.err != nil {
		return b
	}

	if arch != nil {
		b.params.Arch = *arch
	}

	if b.params.Arch == "" {
		b.err = errors.New("arch is not set")
		log.Error().
			Err(b.err).
			Str("arch", b.params.Arch).
			Msg("Arch is not set. Build failed.")
		return b
	}

	// Map `uname -m` to GOARCH
	var goarch string
	goarch, b.err = unameMToGOARCH(b.params.Arch)
	if b.err != nil {
		log.Error().
			Str("uname_m", b.params.Arch).
			Str("goarch", goarch).
			Msg("Failed to map uname -m to GOARCH.")
		return b
	}

	env := "GOARCH"
	b.err = os.Setenv(env, goarch)
	if b.err != nil {
		log.Error().
			Err(b.err).
			Str("env", env).
			Str("value", goarch).
			Msg("Failed to set the GOARCH environmental variable.")
		return b
	}

	return b
}

// SetAgentParams will set the Agent parameters.
func (b *Build) SetAgentParams(ap *AgentParams) *Build {
	if b.err != nil {
		return b
	}

	b.AgentParams = ap
	return b
}

// SetFilename will set the filename according to the build parameters.
func (b *Build) SetFilename() *Build {
	if b.err != nil {
		return b
	}

	var ext = ""
	if b.params.OS == "windows" || b.params.OS == "windows_nt" {
		// Add file extension for Windows
		ext = ".exe"
	}
	// Add the OS and Arch to the filename
	b.params.Filename = fmt.Sprintf("%s_%s_%s%s", b.params.Filename, b.params.OS, b.params.Arch, ext)
	return b
}

// BuildAgent will build the package for deployment to an agent.
func (b *Build) BuildAgent() *Build {
	if b.err != nil {
		return b
	}

	b.GatherInfo().SetOS(nil).SetArch(nil)
	if b.err != nil {
		log.Error().
			Err(b.err).
			Msg("Failed to gather necessary build info.")
		return b
	}

	ldflags := []string{
		"VERSION=" + b.params.Version,
		"BUILD_TIME=" + b.params.BuildTime,
		"BUILD_ENV=" + b.params.BuildEnv,
		"GIT_COMMIT=" + b.params.GitCommit,
		"GIT_TAG=" + b.params.GitTag,
		"GIT_BRANCH=" + b.params.GitBranch,
	}
	var x []string
	for _, ldflag := range ldflags {
		x = append(x, fmt.Sprintf("-X '%s.%s'", modulePrefix, ldflag))
	}
	c := []string{
		"go build",
		"-ldflags \"" + strings.Join(x, " ") + "\"",
		"-tags agent",
		"-o",
		b.params.Filename,
	}
	cmd := strings.Join(c, " ")

	log.Info().
		Str("cmd", cmd).
		Msg("Executing build command.")

	var stdout string
	stdout, b.err = script.Exec(cmd).String()
	if b.err != nil {
		log.Error().
			Err(b.err).
			Str("stdout", stdout).
			Msg("Build has errors.")
	}
	if stdout == "" {
		log.Info().
			Str("stdout", stdout).
			Msg("Output from build process.")
	}
	return b
}

// findGitRoot will find the git root from the current directory.
func (b *Build) findGitRoot() *Build {
	if b.err != nil {
		return b
	}

	// Find the repository root
	var gitRoot string
	gitRoot, b.err = filepath.Abs(".")
	if b.err != nil {
		b.err = errors.New("failed to get the current directory")
		log.Error().
			Err(b.err).
			Msg("Failed to get the current directory.")
		return b
	}
	for count := 20; len(gitRoot) > 1; gitRoot = path.Dir(gitRoot) {
		if ufs.Exists(filepath.Join(gitRoot, ".git")) {
			log.Debug().
				Str("git_root", gitRoot).
				Msg("Found git root.")
			b.GitRoot = gitRoot
			return b
		}
		if count--; count <= 0 {
			break
		}
	}

	// Root wasn't found above. Process the error.
	b.err = errors.New("failed to find the git root")
	log.Error().
		Err(b.err).
		Msg("Failed to find the git root from the current directory.")

	return b
}

// GatherInfo will gather the external info for the build.
func (b *Build) GatherInfo() *Build {
	if b.err != nil {
		return b
	}

	var err error

	r, err := git.PlainOpen(b.GitRoot)
	if err != nil {
		log.Warn().
			Err(err).
			Msg("Failed to open the git repository.")
		return b
	}

	// Get the git hash: `git rev-parse HEAD`
	// Get the current branch: `git rev-parse --abbrev-ref HEAD`
	if ref, err := r.Head(); err != nil {
		log.Warn().
			Err(err).
			Msg("Failed to get HEAD commit hash.")
		b.params.GitCommit = "unknown"
		b.params.GitBranch = "unknown"
	} else {
		b.params.GitCommit = ref.Hash().String()

		s := strings.Split(ref.Name().String(), "/")
		b.params.GitBranch = s[len(s)-1]
	}

	// Get the closest tag to this commit: `git describe --abbrev=0 --tags`
	if tag, err := utils.GetLatestTagFromRepository(r); err != nil {
		log.Warn().
			Err(err).
			Msg("Failed to get the latest tag from the repository.")
		b.params.GitTag = "unknown"
	} else {
		s := strings.Split(tag, "/")
		b.params.GitTag = s[len(s)-1]
	}

	// Use local time with timezone offset from UTC
	//b.params.BuildTime = time.Now().UTC().Format(time.RFC3339)
	b.params.BuildTime = time.Now().Format(time.RFC3339)

	b.SetFilename()
	return b
}
