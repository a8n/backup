// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is not included in the Agent build.
//go:build !agent

// Package manage provides the management interface for the backup system.
package manage

import (
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"gitlab.com/a8n/backup/lib/crypto"

	"github.com/bitfield/script"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclparse"
	"github.com/rs/zerolog/log"
)

// Secrets starts a new Secrets function chain
type Secrets struct {
	Agent    *crypto.KeyPair
	TRMM     *crypto.KeyPair
	Template SecretsTemplate
	err      error
	bytes    []byte
}

// SecretsTemplate represents the template for loading/saving to HCL.
type SecretsTemplate struct {
	Agent KeyPair `hcl:"agent,block" json:"agent"`
	TRMM  KeyPair `hcl:"trmm,block" json:"trmm"`
}

// KeyPair represents the public/private key pair in the HCL template.
type KeyPair struct {
	Public  string `hcl:"public_key" json:"public_key"`
	Private string `hcl:"private_key" json:"private_key"`
}

// secretsTemplate is the default template for trmm.hcl.
//go:embed templates/secrets.hcl
var secretsTemplate []byte

// NewSecrets starts a new encryption request
func NewSecrets() *Secrets {
	return &Secrets{
		Agent: crypto.NewKeyPair("AGENT"),
		TRMM:  crypto.NewKeyPair("TRMM"),
	}
}

// LoadTemplate will load the template secrets.hcl.
func (s *Secrets) LoadTemplate() *Secrets {
	if s.err != nil {
		return s
	}

	log.Debug().
		Int("size", len(secretsTemplate)).
		Msg("Loading secrets template.")

	parser := hclparse.NewParser()
	var diags hcl.Diagnostics
	var f *hcl.File
	f, diags = parser.ParseHCL(secretsTemplate, "secrets.hcl")
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Msg("Failed to parse HCL file.")
		s.err = diags
		return s
	}

	decodeDiags := gohcl.DecodeBody(f.Body, nil, &s.Template)
	diags = append(diags, decodeDiags...)
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Msg("Failed to decode HCL file.")
		s.err = diags
		return s
	}

	// Copy the information to the AgentConfig and TRMM structs from the template.
	// If the default template was loaded, the public and private keys will not exist.
	if s.Template.Agent.Public != "" && s.Template.Agent.Private != "" {
		s.Agent.FromPEM(crypto.PEMPair{
			Public:  s.Template.Agent.Public,
			Private: s.Template.Agent.Private,
		})
	}
	if s.Template.TRMM.Public != "" && s.Template.TRMM.Private != "" {
		s.TRMM.FromPEM(crypto.PEMPair{
			Public:  s.Template.TRMM.Public,
			Private: s.Template.TRMM.Private,
		})
	}

	return s
}

// Load and parse the Secrets HCL file.
func (s *Secrets) Load(filename string) *Secrets {
	if s.err != nil {
		return s
	}

	if len(filename) == 0 {
		log.Error().
			Msg("HCL template file is not specified.")
		s.err = errors.New("secrets hcl template file not specified")
		return s
	}

	var diags hcl.Diagnostics
	parser := hclparse.NewParser()
	file, parseDiags := parser.ParseHCLFile(filename)
	diags = append(diags, parseDiags...)
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Str("filename", filename).
			Msg("Failed to parse secrets HCL file.")
		s.err = diags
		return s
	}

	decodeDiags := gohcl.DecodeBody(file.Body, nil, &s.Template)
	diags = append(diags, decodeDiags...)
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Msg("Failed to decode HCL file.")
		s.err = diags
		return s
	}
	log.Trace().
		Str("filename", filename).
		Str("template", fmt.Sprintf("%#v", s.Template)).
		Msg("Loaded Secrets config.")

	// Copy the information to the data struct from the Secrets HCL file.
	s.Agent.FromPEM(crypto.PEMPair{
		Public:  s.Template.Agent.Public,
		Private: s.Template.Agent.Private,
	})
	if s.Agent.HasErrors() {
		log.Error().
			Err(s.Agent.Error()).
			Msg("Failed to convert PEM loaded from Secrets HCL file to Agent config.")
		return s
	}

	s.TRMM.FromPEM(crypto.PEMPair{
		Public:  s.Template.TRMM.Public,
		Private: s.Template.TRMM.Private,
	})
	if s.TRMM.HasErrors() {
		log.Error().
			Err(s.TRMM.Error()).
			Msg("Failed to convert PEM loaded from Secrets HCL file to TRMM config.")
		return s
	}

	return s
}

// Save the Secrets HCL to a file.
func (s *Secrets) Save(filename string, format string) *Secrets {
	if s.err != nil {
		return s
	}

	// Copy the information to the template from the AgentConfig and TRMM structs.
	var pair crypto.PEMPair
	s.Agent.ToPEM(&pair)
	if s.Agent.HasErrors() {
		log.Error().
			Err(s.Agent.Error()).
			Msg("Failed to convert AgentConfig Secrets to PEM before saving to file.")
		return s
	}
	s.Template.Agent.Public = pair.Public
	s.Template.Agent.Private = pair.Private

	s.TRMM.ToPEM(&pair)
	if s.TRMM.HasErrors() {
		log.Error().
			Err(s.TRMM.Error()).
			Msg("Failed to convert TRMM Secrets to PEM before saving to file.")
		return s
	}
	s.Template.TRMM.Public = pair.Public
	s.Template.TRMM.Private = pair.Private

	var contents string
	var err error
	switch format {
	case "json":
		contents = s.JSON()

	case "hcl":
		contents = s.HCL()

	default:
		contents = s.String()
	}

	if s.err != nil {
		log.Error().
			Err(err).
			Msg("Failed to marshal Secrets contents.")
		return s
	}

	if len(contents) == 0 {
		log.Error().
			Msg("Converting Secrets to string resulted in zero length.")
		s.err = errors.New("converting Secrets to string resulted in zero length")
		return s
	}

	log.Trace().
		//Str("trmm_struct", fmt.Sprintf("%#v", s)).
		Str("trmm_json", contents).
		Msg("TRMM struct contents as a string ")

	var b int64
	b, err = script.Echo(contents).WriteFile(filename)
	if err != nil {
		log.Error().
			Err(s.err).
			Int64("bytes_written", b).
			Str("filename", filename).
			Str("format", format).
			Msg("Failed to write Secrets contents to file.")
		s.err = err
		return s
	}
	if b == 0 {
		log.Error().
			Int64("bytes_written", b).
			Str("filename", filename).
			Str("format", format).
			Msg("File is 0 bytes.")
		s.err = err
		return s
	}

	return s
}

// JSON will marshal the secrets structure to JSON.
func (s *Secrets) JSON() string {
	if s.err != nil {
		return ""
	}

	var err error
	s.bytes, err = json.MarshalIndent(s, "", "  ")
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to Marshal struct to JSON.")
		s.err = err
		return ""
	}

	return string(s.bytes)
}

// HCL will marshal the secrets structure to HCL.
func (s *Secrets) HCL() string {
	if s.err != nil {
		return ""
	}

	f := hclwrite.NewEmptyFile()
	gohcl.EncodeIntoBody(s.Template, f.Body())
	s.bytes = f.Bytes()

	// When the wrong struct is passed, no error is produced and f.Bytes() returns an empty string.
	if len(s.bytes) == 0 {
		log.Info().
			Str("secrets_struct", fmt.Sprintf("%#v", s.Template)).
			Msg("Secrets HCL.")
		log.Warn().
			Str("secrets_struct", fmt.Sprintf("%#v", s.Template)).
			Bytes("bytes", f.Bytes()).
			Int("bytes_length", len(f.Bytes())).
			Msg("Failed to marshal struct to HCL. Returning an empty string.")
	}

	return string(s.bytes)
}

// String will convert the secrets structure to a JSON string.
func (s *Secrets) String() string {
	if s.err != nil {
		return ""
	}

	if len(s.bytes) == 0 {
		log.Error().
			Int("string_length", len(s.bytes)).
			Msg("Failed to convert the secrets structure to a string. String is 0 bytes.")
		return ""
	}

	return string(s.bytes)
}

// GenerateKeys will generate public/private encryption keys and store them in the struct.
func (s *Secrets) GenerateKeys() *Secrets {
	if s.err != nil {
		return s
	}

	// AgentConfig
	s.Agent = crypto.NewKeyPair("AGENT")
	s.Agent.GenerateKeyPair().Marshal()
	if s.Agent.Error() != nil {
		log.Error().
			Err(s.Agent.Error()).
			Msg("Failed to generate AgentConfig public/private key pair.")
		return s
	}
	log.Trace().
		Str("agent_public_pem", s.Agent.Public.String()).
		Str("private_pem", s.Agent.Private.String()).
		Msg("AgentConfig PEM.")

	// TRMM
	s.TRMM.GenerateKeyPair().Marshal()
	if s.TRMM.Error() != nil {
		log.Error().
			Err(s.TRMM.Error()).
			Msg("Failed to generate TRMM public/private key pair.")
		return s
	}

	log.Trace().
		Str("public_pem", s.TRMM.Public.String()).
		Str("private_pem", s.TRMM.Private.String()).
		Msg("TRMM PEM.")

	return s
}

// Errors returns errors present, or nil otherwise.
func (s *Secrets) Error() error {
	return s.err
}

// HasErrors returns true if errors are present.
func (s *Secrets) HasErrors() bool {
	return s.err != nil
}

// ErrorMessage returns the error message.
func (s *Secrets) ErrorMessage() string {
	return s.err.Error()
}
