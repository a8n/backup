// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is not included in the Agent build.
//go:build !agent

// Package manage provides the management interface for the backup system.
package manage

import (
	_ "embed"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/bitfield/script"
	"github.com/go-yaml/yaml"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclparse"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/rs/zerolog/log"
	"gitlab.com/a8n/backup/lib"
)

// Agent starts a new Agent function chain
type Agent struct {
	Template lib.AgentTemplate
	err      error
	bytes    []byte
}

// agentTemplate is the default template for agent.hcl.
//go:embed templates/agent.hcl
var agentTemplate []byte

// NewAgent starts a new request for the backup agent.
func NewAgent() *Agent {
	return &Agent{}
}

// LoadTemplate will load the template agent.hcl embedded into the binary.
func (a *Agent) LoadTemplate() *Agent {
	if a.err != nil {
		return a
	}

	log.Debug().
		Int("size", len(agentTemplate)).
		Msg("Loading agent template.")

	parser := hclparse.NewParser()
	var diags hcl.Diagnostics
	var f *hcl.File
	f, diags = parser.ParseHCL(agentTemplate, "agent.hcl")
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Msg("Failed to parse HCL file.")
		a.err = diags
		return a
	}

	decodeDiags := gohcl.DecodeBody(f.Body, nil, &a.Template)
	diags = append(diags, decodeDiags...)
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Msg("Failed to decode HCL file.")
		a.err = diags
		return a
	}

	return a
}

// Load and parse the Agent HCL file.
func (a *Agent) Load(filename string) *Agent {
	if a.err != nil {
		return a
	}

	if len(filename) == 0 {
		log.Error().
			Msg("AgentConfig template file is not specified.")
		a.err = errors.New("agent template file not specified")
		return a
	}

	var diags hcl.Diagnostics
	parser := hclparse.NewParser()
	file, parseDiags := parser.ParseHCLFile(filename)
	diags = append(diags, parseDiags...)
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Str("filename", filename).
			Msg("Failed to parse AgentConfig HCL file.")
		a.err = diags
		return a
	}

	decodeDiags := gohcl.DecodeBody(file.Body, nil, &a.Template)
	diags = append(diags, decodeDiags...)
	if diags.HasErrors() {
		log.Error().
			Err(diags).
			Msg("Failed to decode HCL file.")
		a.err = diags
		return a
	}
	log.Trace().
		Str("filename", filename).
		Str("template", fmt.Sprintf("%#v", a.Template)).
		Msg("Loaded AgentConfig config.")

	return a
}

// Save the Agent HCL to a file.
func (a *Agent) Save(filename string, format string) *Agent {
	if a.err != nil {
		return a
	}

	var contents string
	var err error
	switch format {
	case "base64":
		contents = a.JSON().encodeBase64()

	case "json":
		contents = a.JSON().String()

	case "hcl":
		contents = a.HCL().String()

	default:
		contents = a.HCL().String()
	}

	if a.err != nil {
		log.Error().
			Err(err).
			Msg("Failed to marshal CryptHCL contents.")
		return a
	}

	if len(contents) == 0 {
		log.Error().
			Err(err).
			Msg("Converting CryptHCL to string resulted in zero length.")
		a.err = errors.New("converting CryptHCL to string resulted in zero length")
		return a
	}

	var b int64
	b, err = script.Echo(contents).WriteFile(filename)
	if err != nil {
		log.Error().
			Err(err).
			Int64("bytes_written", b).
			Str("filename", filename).
			Str("format", format).
			Msg("Failed to write AgentConfig contents to file.")
		a.err = err
		return a
	}
	if b == 0 {
		log.Error().
			Int64("bytes_written", b).
			Str("filename", filename).
			Str("format", format).
			Msg("File is 0 bytes.")
		a.err = err
		return a
	}

	return a
}

// Errors returns errors present, or nil otherwise.
func (a *Agent) Error() error {
	return a.err
}

// HasErrors returns true if errors are present.
func (a *Agent) HasErrors() bool {
	return a.err != nil
}

// ErrorMessage returns the error message.
func (a *Agent) ErrorMessage() string {
	return a.err.Error()
}

/**
 * Template methods
 */

// String will convert the Agent to a string.
func (a *Agent) String() string {
	if a.err != nil {
		return ""
	}

	a.Bytes()
	if a.err != nil {
		return ""
	}

	return string(a.bytes)
}

// Bytes will convert the Agent to a []byte slice.
func (a *Agent) Bytes() []byte {
	if a.err != nil {
		return nil
	}

	if len(a.bytes) == 0 {
		// Default is HCL format
		a.HCL()
		if a.err != nil {
			return nil
		}
	}

	return a.bytes
}

// HCL will convert the Agent to HCL.
func (a *Agent) HCL() *Agent {
	if a.err != nil {
		return a
	}

	f := hclwrite.NewEmptyFile()
	gohcl.EncodeIntoBody(a.Template, f.Body())
	a.bytes = f.Bytes()

	// When the wrong struct is passed, no error is produced and f.Bytes() returns
	// and empty string.
	if len(a.bytes) == 0 {
		log.Debug().
			Str("bytes_length", string(f.Bytes())).
			Msg("Converting to HCL failed. Returning an empty string.")
	}

	return a
}

// JSON will convert the Agent to JSON.
func (a *Agent) JSON() *Agent {
	if a.err != nil {
		return a
	}

	var err error
	a.bytes, err = json.MarshalIndent(a.Template, "", "  ")
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to marshal JSON.")
		a.err = err
		return a
	}

	return a
}

// YAML will convert the Agent to YAML.
func (a *Agent) YAML() *Agent {
	if a.err != nil {
		return a
	}

	var err error
	a.bytes, err = yaml.Marshal(a.Template)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to marshal YAML.")
		a.err = err
		return a
	}

	return a
}

// encodeBase64 will encode the AgentTemplate bytes to base64.
func (a *Agent) encodeBase64() string {
	if a.err != nil {
		return ""
	}

	return base64.StdEncoding.EncodeToString(a.bytes)
}
