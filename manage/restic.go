// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// This file is not included in the Agent build.
//go:build !agent

// Package manage provides the interface for the backup system.
package manage

// Restic config
// See https://restic.readthedocs.io/en/latest/040_backup.html#environment-variables
type Restic struct {
	Repository                   *string `hcl:"repository,attr" json:"repository"`
	Password                     *string `hcl:"password,attr" json:"password,omitempty"` // Encryption password
	KeyHint                      *string `hcl:"key_hint,attr" json:"key_hint,omitempty"`
	OSStorageUrl                 *string `hcl:"os_storage_url,attr" json:"os_storage_url,omitempty"`
	OSAuthToken                  *string `hcl:"os_auth_token,attr" json:"os_auth_token,omitempty"`
	B2AccountID                  *string `hcl:"b2_account_id,attr" json:"b2_account_id,omitempty"`
	B2AccountKey                 *string `hcl:"b2_account_key,attr" json:"b2_account_key,omitempty"`
	AzureAccountName             *string `hcl:"azure_account_name,attr" json:"azure_account_name,omitempty"`
	AzureAccountKey              *string `hcl:"azure_account_key,attr" json:"azure_account_key,omitempty"`
	GoogleProjectID              *string `hcl:"google_project_id,attr" json:"google_project_id,omitempty"`
	GoogleApplicationCredentials *string `hcl:"google_application_credentials,attr" json:"google_application_credentials,omitempty"`
	BWLimit                      *string `hcl:"bw_limit,attr" json:"bw_limit,omitempty"`
}

// Rest config for a rest-server
type Rest struct {
	Host     *string `hcl:"host,attr" json:"host,omitempty"`
	Port     *string `hcl:"port,attr" json:"port,omitempty" default:"443"`
	Username *string `hcl:"username,attr" json:"username,omitempty"`
	Password *string `hcl:"password,attr" json:"password,omitempty"`
	Path     *string `hcl:"path,attr" json:"path,omitempty" default:""`
}

// Backblaze config
type Backblaze struct {
	AccountID  *string `hcl:"b2_account_id,attr" json:"b2_account_id,omitempty"`
	AccountKey *string `hcl:"b2_account_key,attr" json:"b2_account_key,omitempty"`
}

// Azure config
type Azure struct {
	AccountName *string `hcl:"azure_account_name,attr" json:"azure_account_name,omitempty"`
	AccountKey  *string `hcl:"azure_account_key,attr" json:"azure_account_key,omitempty"`
}

// Google config
type Google struct {
	ProjectID              *string `hcl:"google_project_id,attr" json:"google_project_id,omitempty"`
	ApplicationCredentials *string `hcl:"google_application_credentials,attr" json:"google_application_credentials,omitempty"`
}
