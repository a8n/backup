// Copyright 2022, The a8n Authors. All rights reserved.
// SPDX-License-Identifier: PostgreSQL

// Package backup integrates with TRMM to provide backups.
package backup

import (
	"errors"
	"github.com/alecthomas/kong"
	"github.com/rs/zerolog/log"
	ufs "gitlab.com/a8n/backup/lib/fs"
	"os"
	"path/filepath"
)

// CLI contains the command line configuration
type CLI struct {
	// Manage is the user facing function for managing the backup configs
	Manage struct {
		ConfigDir string `kong:"name='data',group='manage',required='',placeholder='/secure/dir',help='Directory of the config files.'"`
		// FIXME: existingfile or existingdir with a default value will check for the file/dir even if another
		// command is run. Remove the default value for now.
		// https://github.com/alecthomas/kong/issues/26
		AgentConfig string `kong:"name='agent-config',group='manage',required='',placeholder='client-site-application.hcl',help='AgentConfig config file to load.'"`
		//LoadSecrets   string `kong:"name='load-secrets',group='manage',required='',xor='secrets',type='existingfile',placeholder='data/secrets.hcl',help='Existing encryption config file to load. Mutually exclusive with --save-crypt.'"`
		//SaveSecrets   string `kong:"name='save-secrets',group='manage',required='',xor='secrets',placeholder='data/secrets.hcl',help='Encryption config file to save. Mutually exclusive with --load-secrets.'"`
		//SaveTRMM      string `kong:"name='save-trmm',group='manage',required='',default='${data}/trmm.json.base64',placeholder='data/trmm.json.base64',help='TRMM config file to save.'"`
	} `kong:"cmd='',group='manage',help='Manage the backup configuration.'"`

	// Currently not used
	TRMM struct {
		Client string `kong:"name='client',optional,help='TRMM Client'"`
		Site   string `kong:"name='site',optional,help='TRMM Site'"`
		Agent  string `kong:"name='agent',optional,help='TRMM AgentConfig'"`
		Script string `kong:"name='script',arg='script',help='Script to output.'"`

		/*
			// Download is used to match the client computer to the hosted binary.
			Download struct {
				OS   string `kong:"name='os',optional,default='',help='OS for the script.'"`
				Arch string `kong:"name='arch',optional,default='',help='Architecture for the script.'"`
			} `kong:"name='download',cmd=''"`
		*/
	} `kong:"cmd='',group='trmm',help='Tactical RMM functions.'"`

	// Run will run the backup
	Agent struct {
		Backup struct {
			Paths []string `kong:"arg='',optional='',group='agent',help='restic command'"`
		} `kong:"cmd='',group='agent',help='Run the backup'"`
		Restic struct {
			Command []string `kong:"arg='',group='agent',help='restic command'"`
		} `kong:"cmd='',group='agent',help='Run a restic command'"`
	} `kong:"cmd='',group='agent',help='Commands to be run on the agents.'"`

	// Logging options
	Logging struct {
		// https://github.com/rs/zerolog#leveled-logging
		Level string `kong:"enum='trace,debug,info,warn,error',default='info',help='Log level'"`
		Type  string `kong:"enum='json,console',default='console',help='Log type'"`
	} `kong:"embed='',prefix='log-'"`

	// Version
	Version struct {
	} `kong:"cmd='',group='version',name='version'"`

	// License
	License struct {
	} `kong:"cmd='',group='license',name='license'"`

	// FileRepo
	FileRepo struct {
	} `kong:"cmd='',group='filerepo',name='filerepo'"`
}

// kongContext is Kong's context
var kongContext *kong.Context

// NewCLI starts a new configuration.
func NewCLI() *CLI {
	return &CLI{}
}

// ParseCLI command line options
func (cli *CLI) ParseCLI() (err error) {
	kongContext = kong.Parse(cli,
		kong.Name("a8n-backup"),
		kong.Description("a8n backup service with TRMM integration"),
		kong.UsageOnError(),
		kong.Vars{
			"data": "data",
		},
		kong.ConfigureHelp(kong.HelpOptions{
			Summary: false,
			//Compact:             true,
			//NoExpandSubcommands: true,
		}))

	SetLogging(&LoggingConfig{
		Level: cli.Logging.Level,
		Type:  cli.Logging.Type,
	})

	log.Debug().
		Msg("Parsing CLI options.")

	err = cli.ValidateCLI()
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to validate CLI commands.")
		return err
	}

	return nil
}

// ValidateCLI will validate the config (outside Kong) and set sane defaults.
// Conflicts with "Validate" since it will be called by Kong.
// See https://github.com/alecthomas/kong#validation
// Kong checks if the file exists with the type='existingfile' struct tag
func (cli *CLI) ValidateCLI() (err error) {
	if kongContext.Command() != "manage" {
		// Validate only the manage command
		log.Debug().
			Str("command", kongContext.Command()).
			Msg("Validating CLI")
		return nil
	}

	if !ufs.Exists(cli.Manage.ConfigDir) || !ufs.IsDir(cli.Manage.ConfigDir) {
		err = errors.New("config directory does not exist")
		log.Error().
			Err(err).
			Str("config_dir", cli.Manage.ConfigDir).
			Msg("Config directory does not exist.")
		return err
	}

	if !ufs.Exists(filepath.Join(cli.Manage.ConfigDir, cli.Manage.AgentConfig)) {
		err = errors.New("agent config does not exist")
		log.Error().
			Err(err).
			Str("data_dir", cli.Manage.ConfigDir).
			Str("agent_config", cli.Manage.AgentConfig).
			Str("agent_path", filepath.Join(cli.Manage.ConfigDir, cli.Manage.AgentConfig)).
			Msg("AgentConfig config does not exist in the data directory.")
		return err
	}

	return nil
}

// createDataDir will create the data directory for the files.
func (cli *CLI) createDataDir() (err error) {
	mode := os.ModeDir | ufs.OS_USER_RWX
	if !ufs.Exists(cli.Manage.ConfigDir) {
		err = os.Mkdir(cli.Manage.ConfigDir, mode)
		if err != nil {
			log.Error().
				Err(err).
				Msg("Failed to create the data directory.")
			return err
		}
	}
	// Change the permissions if possible.
	err = os.Chmod(cli.Manage.ConfigDir, mode)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to chmod the data directory.")
		return err
	}
	return nil
}
