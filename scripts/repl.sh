#!/usr/bin/env bash
# SPDX-License-Identifier: PostgreSQL

# Ingest dotenv
export $(grep -vE "^(#.*|\s*)$" .env)

go build
./backup manage --log-level trace \
  --data "${A8N_BACKUP_DATA}" \
  --agent-config "${A8N_BACKUP_CONFIG}.hcl"

[[ ! -f "a8n-backup_Linux_x86_64" ]] && exit 1
#[[ ! -f "a8n-backup_Windows_amd64" ]] && exit 1
echo -e "\n"

trmm=$(cat "${A8N_BACKUP_DATA}/${A8N_BACKUP_CONFIG}.trmm.json.base64")

# Variables to pass to the backup agent.
BACKUP_PUBLIC_KEY=$(echo "${trmm}" | base64 --decode | jq -r '. .custom_fields.public_key')
BACKUP_PAYLOAD=$(echo "${trmm}" | base64 --decode  | jq -r '. .custom_fields.encrypted')
export BACKUP_PUBLIC_KEY
export BACKUP_PAYLOAD

../../manage/scripts/a8n-backup-linux.sh "${trmm}" "$@"
